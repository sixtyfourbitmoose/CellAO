﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using CellAO.Studio.Extensions;
using System.Collections.ObjectModel;
using System.Diagnostics;
using CellAO.Enums;
using System.Windows.Threading;
using System.Windows.Data;
using Microsoft.Win32;

namespace CellAO.Studio
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<ProcessItem> RunningProcs { get; set; }
        public ObservableCollection<AOInject.DataClass> Messages { get; set; }
        public ObservableCollection<AOInject.DataClass> FilteredMessages { get; set; }
        public ObservableCollection<Messages.N3MessageType> MessageTypes { get; set; }

        public DispatcherTimer dispatchTimer { get; set; }
        public bool Paused { get; private set; }

        public MainWindow()
        {
            RunningProcs = new ObservableCollection<ProcessItem>();
            Messages = new ObservableCollection<AOInject.DataClass>();
            FilteredMessages = new ObservableCollection<AOInject.DataClass>();
            MessageTypes = new ObservableCollection<Messages.N3MessageType>();
            dispatchTimer = new DispatcherTimer();

            InitializeComponent();


            dispatchTimer.Interval = new TimeSpan(0,0,0,5);
            dispatchTimer.Tick += DispatchTimer_Tick;
            dispatchTimer.IsEnabled = true;


            foreach (Messages.N3MessageType s in Enum.GetValues(typeof(Messages.N3MessageType)))
            {
                MessageTypes.Add(s);
            }

            MessageTypes = new ObservableCollection<Messages.N3MessageType>(MessageTypes.OrderBy(m=>m.ToString()).ToList());

            listMessageTypes.ItemsSource = MessageTypes;
            listProcesses.ItemsSource = RunningProcs;
            listMessages.ItemsSource = Messages;

            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(listMessages.ItemsSource);
            view.SortDescriptions.Add(new SortDescription("TimeStamp", ListSortDirection.Descending));
            //view.Filter = MessageFilter;

            DispatchTimer_Tick(this, null);

            BtnPause_Click(this, null);

            AOInject.HookInterface.OnIncomingMessageReceived += HookInterface_OnIncomingMessageReceived;
        }

        public static void AddOnUI<T>(ObservableCollection<T> collection, T item)
        {
            Action<T> addMethod = collection.Add;
            Application.Current.Dispatcher.BeginInvoke(addMethod, item);
        }

        private bool MessageFilter(object item)
        {
            var message = (AOInject.DataClass)item;
            return message.Message() != null 
                && (string.IsNullOrEmpty(txtFilter.Text) || Utility.HexOutput.Output(message.DataBytes).Contains(txtFilter.Text))
                && (listMessageTypes.SelectedItems.Count == 0 || listMessageTypes.SelectedItems.Cast<Messages.N3MessageType>().Where(m => message.ToStringShort().Contains(m.ToString())).Count() > 0);
        }

        private void HookInterface_OnIncomingMessageReceived(object sender, AOInject.DataClass data)
        {
            if (!Paused)
                AddOnUI(Messages, data);
        }

        private void DispatchTimer_Tick(object sender, EventArgs e)
        {
            RunningProcs.Clear();
            foreach (var p in Process.GetProcessesByName("AnarchyOnline").Select(p => new ProcessItem() { Display = p.ProcessName + " - " + p.Id, Value = p }).ToList())
            {
                RunningProcs.Add(p);
            }
            
        }

        private void ListMessageTypes_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(listMessages.ItemsSource).Refresh();
        }

        private void ListView_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (listMessages.SelectedItems.Count == 1 || listMessages.SelectedItems.Count == 2)
            {
                txtResults.Text = ((AOInject.DataClass)listMessages.SelectedItems[0]).Message().DebugStringComplete() + Utility.HexOutput.Output(((AOInject.DataClass)listMessages.SelectedItems[0]).DataBytes);

                if (listMessages.SelectedItems.Count == 2)
                    txtResults2.Text = ((AOInject.DataClass)listMessages.SelectedItems[1]).Message().DebugStringComplete() + Utility.HexOutput.Output(((AOInject.DataClass)listMessages.SelectedItems[1]).DataBytes);
            } else
            {
                txtResults.Text = "";
                txtResults2.Text = "";
            }

        }

        private void ListProcesses_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (((ProcessItem)listProcesses.SelectedValue != null))
                AOHook.Inject(((ProcessItem)listProcesses.SelectedValue).Value.Id);
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            Messages.Clear();
            txtResults.Text = "";
        }

        private void BtnPause_Click(object sender, RoutedEventArgs e)
        {
            if (Paused)
            {
                Paused = false;
                btnPause.Header = "Pause Collection";
            }
            else
            {
                btnPause.Header = "Start Collection";
                Paused = true;
            }
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new SaveFileDialog();
            dialog.DefaultExt = ".json"; // Default file extension
            dialog.Filter = "JSON (.json)|*.json"; // Filter files by extension
            if (dialog.ShowDialog() == true)
            {
                System.IO.File.WriteAllText(dialog.FileName, Newtonsoft.Json.JsonConvert.SerializeObject(Messages));
            }
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                DefaultExt = ".json", // Default file extension
                Filter = "JSON (.json)|*.json" // Filter files by extension
            };
            if (dialog.ShowDialog() == true)
            {
                Messages.Clear();
                foreach(var dc in Newtonsoft.Json.JsonConvert.DeserializeObject<ObservableCollection<AOInject.DataClass>>(System.IO.File.ReadAllText(dialog.FileName)))
                {
                    Messages.Add(dc);
                }
            }
        }

        private void BtnClearUnslected_Click(object sender, RoutedEventArgs e)
        {
            var temp = listMessages.SelectedItems.Cast<AOInject.DataClass>().ToList();
            Messages.Clear();
            foreach(AOInject.DataClass t in temp)
            {
                Messages.Add(t);
            }
        }

        private void BtnClearSelected_Click(object sender, RoutedEventArgs e)
        {
            var temp = listMessages.SelectedItems.Cast<AOInject.DataClass>().ToList();
            var all = Messages.ToList();
            Messages.Clear();
            foreach (AOInject.DataClass t in all)
            {
                if(!temp.Contains(t))
                    Messages.Add(t);
            }
        }

        private void TxtFilter_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(listMessages.ItemsSource).Refresh();
        }

        private void btnLoadExisting_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.DefaultExt = ".json"; // Default file extension
            dialog.Filter = "JSON (.json)|*.json"; // Filter files by extension
            if (dialog.ShowDialog() == true)
            {
                foreach (var dc in Newtonsoft.Json.JsonConvert.DeserializeObject<ObservableCollection<AOInject.DataClass>>(System.IO.File.ReadAllText(dialog.FileName)))
                {
                    Messages.Add(dc);
                }
            }
        }
    }
}
