﻿using System;
using System.Globalization;
using CellAO.Messages.N3Messages;

namespace SCFUPacketFlagViewer
{
    internal static class Program
    {
        private static void Main()
        {
            while (true)
            {
                Console.Write("Input packet flags (as hex):");
                string line = Console.ReadLine();

                if (line.Length == 0)
                {
                    break;
                }

                line = line.Replace(" ", null);

                uint packetFlags;
                if (!UInt32.TryParse(line, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out packetFlags))
                {
                    Console.WriteLine("Invalid hexadecimal number.");
                    continue;
                }

                SimpleCharFullUpdateFlags fl = (SimpleCharFullUpdateFlags)packetFlags;

                SimpleCharFullUpdateFlags[] flags = (SimpleCharFullUpdateFlags[])Enum.GetValues(typeof(SimpleCharFullUpdateFlags));
                foreach (SimpleCharFullUpdateFlags flag in flags)
                {
                    if (fl.HasFlag(flag))
                    {
                        Console.WriteLine($"{((int)flag).ToString("X8")} flag");
                    }
                }
            }
        }
    }
}
