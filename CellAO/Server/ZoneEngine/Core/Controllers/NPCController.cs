﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace ZoneEngine.Core.Controllers
{
    #region Usings ...

    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;

    using CellAO.Core.Entities;
    using CellAO.Core.Functions;
    using CellAO.Core.Network;
    using CellAO.Core.Vector;
    using CellAO.Enums;
    using CellAO.Interfaces;
    using CellAO.ObjectManager;

    using CellAO.Messages.Fields;
    using CellAO.Messages.N3Messages;

    using Utility;

    using ZoneEngine.Core.Functions;
    using ZoneEngine.Core.KnuBot;
    using ZoneEngine.Core.MessageHandlers;
    using CellAO.Core.Nanos;

    using Quaternion = CellAO.Core.Vector.Quaternion;
    using Vector3 = CellAO.Core.Vector.Vector3;
    using CellAO.Stats;

    #endregion

    /// <summary>
    /// </summary>
    public class NPCController : BaseController, IController
    {
        public CellAO.Core.Combat.IAIPattern AIPattern { get; set; }
        public int? MobSpawnId { get; set; }
        public BaseKnuBot KnuBot = null;

        private double lastDistance = double.MaxValue;

        private Identity followIdentity = Identity.None;

        private Vector3 followCoordinates = new Vector3();

        private CharacterState state = CharacterState.Idle;

        private int activeWaypoint = 0;

        public CharacterState State
        {
            get
            {
                return this.state;
            }
            set
            {
                this.state = value;
            }
        }
        public override ICharacter Character
        {
            get
            {
                return this._character;
            }
            set
            {
                this.AIPattern = new CellAO.Core.Combat.BasicAttackerAI(value);
                this._character = value;
            }
        }



        public static void SendMobSpawnMessages(ICharacter mobCharacter)
        {

            // Add default weapon
            mobCharacter.BaseInventory.TryAdd(new CellAO.Core.Items.Item(1, 121567, 121567), (int)IdentityType.WeaponPage, 6);

            SimpleCharFullUpdateMessage mess = ZoneEngine.Core.Packets.SimpleCharFullUpdate.ConstructMessage(mobCharacter as Character);
            mobCharacter.Playfield.Announce(mess);
            AppearanceUpdateMessageHandler.Default.Send(mobCharacter);

            uint ammo = (uint)mobCharacter.GetRightHandWeapon().GetAttribute((int)StatIds.energy);
            ammo = (ammo == Stat.BASE_VALUE) ? uint.MaxValue : ammo;

            mobCharacter.Playfield.Announce(new WeaponItemFullUpdateMessage(

                    mobCharacter.GetRightHandWeapon().Identity,
                    mobCharacter.Identity,
                    mobCharacter.Playfield.Identity,
                    new Identity() { Instance = 1, Type = IdentityType.UnknownRDB1 },
                    (byte)6,
                    (uint)Convert.ToUInt32(mobCharacter.GetRightHandWeapon().Flags),
                    (uint)mobCharacter.GetRightHandWeapon().LowID,
                    (uint)mobCharacter.GetRightHandWeapon().Quality,
                    (uint)mobCharacter.GetRightHandWeapon().LowID,
                    (uint)mobCharacter.GetRightHandWeapon().HighID,
                    (uint)0x00000001,
                    (uint)ammo,
                    (uint)mobCharacter.GetRightHandWeapon().GetAttribute((int)StatIds.itemdelay),
                    (uint)mobCharacter.GetRightHandWeapon().GetAttribute((int)StatIds.rechargedelay)
                ));
            System.Threading.Thread.Sleep(50);

            mobCharacter.Playfield.Announce(new SpecialAttackWeaponMessage()
            {
                Identity = mobCharacter.Identity,
                Specials = (new System.Collections.Generic.List<SpecialAttack>()
                            {
                              new SpecialAttack()
                              {
                                  Unknown1 = 121041 ,//client.Controller.Character.GetRightHandWeapon().LowID,
                                  Unknown2 = 121041 ,//client.Controller.Character.GetRightHandWeapon().HighID,
                                  Unknown3 = 1380276018,
                                  Unknown4 = "REW2"
                              },
                              new SpecialAttack()
                              {
                                  Unknown1 = 121038 ,//client.Controller.Character.GetRightHandWeapon().LowID,
                                  Unknown2 = 121038 ,//client.Controller.Character.GetRightHandWeapon().HighID,
                                  Unknown3 = 1380276017 ,
                                  Unknown4 = "REW1"
                              },
                            }).ToArray(),
                AggDef = 0
            });
        }

        public void Die()
        {
            if (this.MobSpawnId != null) this.Character.Playfield.RecordMobDeath(this.MobSpawnId.Value);
            Utility.LogUtil.Debug(DebugInfoDetail.Error, Character.Name + " has been killed.");

            if (AIPattern.HateList.Count > 0)
            {

                KeyValuePair<int, int> highestHate = new KeyValuePair<int, int>(0, -1);

                foreach (var hate in AIPattern.HateList)
                {
                    if (hate.Value >= highestHate.Value)
                    {
                        highestHate = hate;
                    }
                }

                var attacker = CellAO.Core.Entities.Character.GetCharacter(highestHate.Key);
                var attackersTeam = Team.GetCharacterTeam(highestHate.Key);


                if (attackersTeam != null)
                {
                    foreach (var teamMember in attackersTeam.Characters)
                    {
                        CellAO.Core.Entities.Character.GetCharacter(teamMember).Controller.AwardXp(Character.Controller);
                    }
                }
                else
                {
                    attacker.Controller.AwardXp(Character.Controller);
                }
            }


            this.Character.DoStopAttack();
            this.StopFollow();
            this.SendDeathAnimation();
            this.Character.ActionsQueue.InsertRange(0, new List<CellAO.Core.CharacterAction>() {
                        new CellAO.Core.CharacterAction()
                        {
                            Character = this.Character,
                            AttackCycleRemaining = 4000,
                            RechargeCycleRemaining = 0,
                            Id = Character.Identity.Instance,
                            Type = CellAO.Core.CharacterActionTypes.Despawn
                        },
                        new CellAO.Core.CharacterAction()
                        {
                            Character = Character,
                            AttackCycleRemaining = 1,
                            RechargeCycleRemaining = 0,
                            Id = Character.Identity.Instance,
                            Type = CellAO.Core.CharacterActionTypes.Dispose
                        }
                    });
            System.Threading.Thread.Sleep(1000);
            this.Character.Dispose();
            this.Dispose();
        }

        public void SendDeathAnimation()
        {
            Character.Playfield.Announce(new CharacterActionMessage()
            {
                Identity = Character.Identity,
                Parameter2 = 0x000001F7,
                Action = CharacterActionType.DeathAnimation
            });
        }

        // Always null here
        public IZoneClient Client
        {
            get
            {
                return null;
            }
            set
            {
                throw new Exception("NPC's dont have a client. Faulty code tries to use it!!");
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool IsDead()
        {
            return this.Character.Stats[StatIds.health].Value <= 0;
        }

        public void ApplyDamage(int amount, ICharacter source, int slot, bool isCrit = false)
        {
            try
            {
                int oldVal = this.Character.Stats[StatIds.health].Value;
                int newVal = oldVal - amount;
                // Console.WriteLine(String.Format("{0}: HP [{1}/{2}]", this.Character.Name, newVal, oldVal));
                newVal = newVal < 0 ? 0 : newVal;
                this.Character.Stats[StatIds.health].Set(Convert.ToUInt32(newVal));

                var item = slot == 6 || slot == 0 ? source.GetRightHandWeapon() : source.GetLeftHandWeapon(); // slot 0 is unarmed
                bool isUnarmed = source.GetRightHandWeapon().InitiativeType == InitiativeType.PhysicalInit;
                int Unknown6 = item.InitiativeType == InitiativeType.RangedInit ? 0x00000000 : 0x00000064; // MELEE vs RANGED ??

                uint Unknown2 = (uint)item.GetAttribute((int)StatIds.energy);

                this.Character.Send(new AttackInfoMessage()
                {
                    Unknown = 0x00,
                    Identity = source.Identity,
                    Target = this.Character.Identity,
                    DamageAmount = Convert.ToUInt32(amount),
                    Unknown6 = 0x00000064, //Unknown6, //0x00000064,
                    Unknown5 = isCrit ? 0x00000004 : 0x00000003, // 4 is crit 3 is normal?
                    Unknown4 = newVal <= 0 ? 0x00000004 : 0x00000000,
                    Unknown2 = Unknown2,
                    Unknown3 = slot
                });
                source.Send(new AttackInfoMessage()
                {
                    Unknown = 0x00,
                    Identity = source.Identity,
                    Target = this.Character.Identity,
                    DamageAmount = Convert.ToUInt32(amount),
                    Unknown6 = 0x00000064, //Unknown6, //0x00000064,
                    Unknown5 = isCrit ? 0x00000004 : 0x00000003, // 4 is crit 3 is normal?
                    Unknown4 = newVal <= 0 ? 0x00000004 : 0x00000000,
                    Unknown2 = Unknown2,
                    Unknown3 = slot
                });

                this.AIPattern.AddHate(source, amount);
            }
            catch (Exception ex)
            {
                Utility.LogUtil.WarnException(ex);
            }

        }

        public bool LookAt(Identity target)
        {
            throw new NotImplementedException();
        }

        public bool UseStatel(Identity identity, EventType eventType = EventType.OnUse)
        {
            throw new NotImplementedException();
        }

        public void SendChatText(string text)
        {
            throw new NotImplementedException();
        }



        public void FinishCastingNano(CellAO.Core.CharacterAction action)
        {

            // Check here for nanoresist of the target, maybe the 1 in finishnanocasting is kind of did land/didnt land flag
            CharacterActionMessageHandler.Default.FinishNanoCasting(
                this.Character,
                CharacterActionType.FinishNanoCasting,
                Identity.None,
                1,
                action.Nano.ID);

            // TODO: Calculate nanocost modifiers etc.
            this.Character.Stats[StatIds.currentnano].Value -= action.Nano.getItemAttribute(407);

            // CharacterAction 98 - Set nano duration
            CharacterActionMessageHandler.Default.SetNanoDuration(
                this.Character,
                action.TargetCharacter.Identity,
                action.Nano.ID,
                action.Nano.getItemAttribute(8));

            action.Nano.Events.ForEach(evt => evt.Functions.ForEach(func => action.TargetCharacter.Controller.CallFunction(func, Character)));
        }

        /// <summary>
        /// </summary>
        /// <param name="nanoId">
        /// </param>
        /// <param name="target">
        /// </param>
        /// <returns>
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool CastNano(int nanoId, Identity target)
        {
            // Procedure:
            // 1. Check if nano can be casted (criteria to Use (3))
            // 2. Lock nanocasting ability
            // 3. Wait for cast attack delay
            // 4. Check target's restance to the nano
            // 5. Execute nanos gamefunctions
            // 6. Wait for nano recharge delay
            // 7. Unlock nano casting

            NanoFormula nano = NanoLoader.NanoList[nanoId];

            int strain = nano.NanoStrain();

            Character targetChar = Pool.Instance.GetAll<Character>((int)IdentityType.Dynel).Where(c => c.Identity.Instance == target.Instance).FirstOrDefault();

            CastNanoSpellMessageHandler.Default.Send(this.Character, nanoId, target);

            // CharacterAction 107 - Finish nano casting
            int attackDelay = this.Character.CalculateNanoAttackTime(nano);
            Console.WriteLine("Attack-Delay: " + attackDelay);

            this.Character.ActionsQueue.InsertRange(0, new List<CellAO.Core.CharacterAction>() {
                        new CellAO.Core.CharacterAction()
                        {
                            Character = this.Character,
                            AttackCycleRemaining = attackDelay * 10,
                            RechargeCycleRemaining = nano.getItemAttribute(210) * 10,
                            Id = Character.Identity.Instance,
                            Type = CellAO.Core.CharacterActionTypes.CastNano,
                            TargetCharacter = targetChar,
                            Nano = nano
                        }
                    });
            CellAO.Core.CharacterActionManager.Start();

            return false;
        }


        public bool Search()
        {
            throw new NotImplementedException();
        }

        public bool Sneak()
        {
            throw new NotImplementedException();
        }

        public bool ChangeVisualFlag(int visualFlag)
        {
            throw new NotImplementedException();
        }

        public bool Move(int moveType, Coordinate newCoordinates, Quaternion heading)
        {
            throw new NotImplementedException();
        }

        public bool ContainerAddItem(int sourceContainerType, int sourcePlacement, Identity target, int targetPlacement)
        {
            throw new NotImplementedException();
        }

        public bool Follow(Identity target)
        {
            try
            {
                this.followIdentity = target;
                ICharacter npc = Pool.Instance.GetObject<ICharacter>(this.Character.Playfield.Identity, target);
                if (npc != null)
                {
                    Vector3 temp = npc.Coordinates().coordinate - this.Character.Coordinates().coordinate;
                    temp.y = 0;
                    if (temp.Magnitude == 0) return false;
                    this.Character.Heading = (Quaternion)Quaternion.GenerateRotationFromDirectionVector(temp).Normalize();
                    FollowTargetMessageHandler.Default.Send(
                        this.Character,
                        this.Character.Coordinates().coordinate,
                        npc.Coordinates().coordinate);
                    this.Run();
                    this.StartMovement();
                }
            }
            catch (Exception ex)
            {
                Utility.LogUtil.WarnException(ex);
            }
            return true;
        }

        public bool Stand()
        {
            throw new NotImplementedException();
        }

        public bool SocialAction(
            SocialAction action,
            byte parameter1,
            byte parameter2,
            byte parameter3,
            byte parameter4,
            int parameter5)
        {
            throw new NotImplementedException();
        }

        public bool Trade(Identity target)
        {
            // Do we have a attached KnuBot?
            if ((this.KnuBot != null) && (this.KnuBot.Character.Target == null))
            {
                return
                    this.KnuBot.StartDialog(
                        Pool.Instance.GetObject<ICharacter>(this.Character.Playfield.Identity, target));
            }
            return false;
        }

        public bool UseItem(Identity itemPosition)
        {
            throw new NotImplementedException();
        }

        public bool DeleteItem(int container, int slotNumber)
        {
            throw new NotImplementedException();
        }

        public bool SplitItemStack(Identity targetItem, int stackCount)
        {
            throw new NotImplementedException();
        }

        public bool JoinItemStack(Identity sourceItem, Identity targetItem)
        {
            throw new NotImplementedException();
        }

        public bool CombineItems(Identity sourceItem, Identity targetItem)
        {
            throw new NotImplementedException();
        }

        public bool TradeSkillSourceChanged(int inventoryPageId, int slotNumber)
        {
            throw new NotImplementedException();
        }

        public bool TradeSkillTargetChanged(int inventoryPageId, int slotNumber)
        {
            throw new NotImplementedException();
        }

        public bool TradeSkillBuildPressed(Identity targetItem)
        {
            throw new NotImplementedException();
        }

        public bool ChatCommand(string command, Identity target)
        {
            throw new NotImplementedException();
        }

        public bool Logout()
        {
            throw new NotImplementedException();
        }

        public void LogoffCharacter()
        {
        }

        public bool Login()
        {
            throw new NotImplementedException();
        }

        public bool StopLogout()
        {
            throw new NotImplementedException();
        }

        public bool GetTargetInfo(Identity target)
        {
            throw new NotImplementedException();
        }

        public bool TeamInvite(Identity target)
        {
            throw new NotImplementedException();
        }

        public bool TeamKickMember(Identity target)
        {
            throw new NotImplementedException();
        }

        public bool TeamLeave()
        {
            throw new NotImplementedException();
        }

        public bool TransferTeamLeadership(Identity target)
        {
            throw new NotImplementedException();
        }

        public bool TeamJoinRequest(Identity target)
        {
            throw new NotImplementedException();
        }

        public bool TeamJoinReply(bool accept, Identity requester)
        {
            throw new NotImplementedException();
        }

        public bool TeamJoinAccepted(Identity newTeamMember)
        {
            throw new NotImplementedException();
        }

        public bool TeamJoinRejected(Identity rejectingIdentity)
        {
            throw new NotImplementedException();
        }

        public void SendChangedStats()
        {
            Dictionary<int, uint> toPlayfield = new Dictionary<int, uint>();
            Dictionary<int, uint> toPlayer = new Dictionary<int, uint>();

            this.Character.Stats.GetChangedStats(toPlayer, toPlayfield);
            toPlayer.Clear();
            StatMessageHandler.Default.SendBulk(this.Character, toPlayer, toPlayfield);
        }

        public void MoveTo(CellAO.Messages.Fields.Vector3 destination)
        {
            FollowTargetMessageHandler.Default.Send(this.Character, this.Character.RawCoordinates, destination);
            Vector3 dest = destination;
            Vector3 start = this.Character.RawCoordinates;
            dest = start - dest;
            dest = dest.Normalize();
            this.Character.Heading = (Quaternion)Quaternion.GenerateRotationFromDirectionVector(dest);
            this.Run();

            Coordinate c = new Coordinate(destination);
            this.followCoordinates = c.coordinate;
            /*bool arrived = false;
            double lastDistance = double.MaxValue;
            while (!arrived)
            {
                Coordinate temp = this.Character.Coordinates;
                double distance = this.Character.Coordinates.Distance2D(c);
                arrived = (distance < 0.2f) || (lastDistance < distance);
                lastDistance = distance;
                // LogUtil.Debug(DebugInfoDetail.Movement,"Moving...");
                Thread.Sleep(100);
            }
            LogUtil.Debug(DebugInfoDetail.Movement, "Arrived at "+this.Character.Coordinates.ToString());
            this.StopMovement();*/
        }

        public void DoFollow()
        {
            Coordinate sourceCoord = this.Character.Coordinates();
            Vector3 targetPosition = this.followCoordinates;
            if (!this.followIdentity.Equals(Identity.None))
            {
                ICharacter targetChar = Pool.Instance.GetObject<ICharacter>(
                    this.Character.Playfield.Identity,
                    this.followIdentity);
                if (targetChar == null)
                {
                    // If target does not longer exist (death or zone or logoff) then stop following
                    this.followIdentity = Identity.None;
                    this.followCoordinates = new Vector3();
                    return;
                }

                targetPosition = targetChar.Coordinates().coordinate;
            }

            // Do we have coordinates to follow?
            if (targetPosition.Distance2D(new Vector3()) < 0.01f)
            {
                return;
            }

            // /!\ If target flies away, there has to be some kind of adjustment
            Vector3 start = sourceCoord.coordinate;
            Vector3 dest = targetPosition;

            // Check if we have arrived
            if (start.Distance2D(dest) < 0.3f)
            {
                this.StopMovement();
                this.Character.RawCoordinates = dest;
                FollowTargetMessageHandler.Default.Send(this.Character, dest);
                this.followCoordinates = new Vector3();
                return;
            }

            LogUtil.Debug(DebugInfoDetail.Movement, "Distance to target: " + start.Distance2D(dest).ToString());

            // If target moved or first call, then issue a new follow
            if (targetPosition.Distance2D(this.followCoordinates) > 2.0f)
            {
                this.StopMovement();
                this.Character.Coordinates(start);
                FollowTargetMessageHandler.Default.Send(this.Character, start);
                Vector3 temp = start - dest;
                temp.y = 0;
                this.Character.Heading = (Quaternion)Quaternion.GenerateRotationFromDirectionVector(temp);
                this.followCoordinates = dest;
                FollowTargetMessageHandler.Default.Send(this.Character, start, dest);
                this.StartMovement();
            }
        }

        public void StartPatrolling()
        {
            Waypoint next = this.FindNextWaypoint();

            // If a suitable waypoint is found
            if (next != null)
            {
                if (next.Running)
                {
                    this.Run();
                }
                else
                {
                    this.Walk();
                }
                this.followCoordinates = next.Position;
                Vector3 temp = this.Character.Coordinates().coordinate - next.Position;
                temp.y = 0;
                this.Character.Heading = (Quaternion)Quaternion.GenerateRotationFromDirectionVector(temp).Normalize();
                LogUtil.Debug(DebugInfoDetail.Movement, "Direction: " + this.Character.Heading.ToString());
                FollowTargetMessageHandler.Default.Send(
                    this.Character,
                    this.Character.Coordinates().coordinate,
                    next.Position);
                this.StartMovement();
                LogUtil.Debug(DebugInfoDetail.Movement, "Walking to: " + this.followCoordinates);
            }
        }

        public bool IsFollowing()
        {
            return ((!this.followIdentity.Equals(Identity.None)) || (this.followCoordinates.x != 0.0f)
                    || (this.followCoordinates.y != 0.0f) || (this.followCoordinates.z != 0.0f));
        }

        public void Run()
        {
            this.Character.UpdateMoveType(25); // Magic number: Switch to run
        }

        public void StopMovement()
        {
            this.Character.UpdateMoveType(2); // Magic number: Forward stop
        }

        public void Walk()
        {
            this.Character.UpdateMoveType(24); // Magic number: Switch to walk
        }

        public bool SaveToDatabase
        {
            get
            {
                return false;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.Client != null)
                {
                    this.Client = null;
                }
            }
        }

        private Waypoint FindNextWaypoint()
        {
            Waypoint result = null;
            if (this.Character.Waypoints.Count <= 2)
            {
                return null;
            }
            if (this.Character.Waypoints.Count <= this.activeWaypoint)
            {
                this.activeWaypoint = 0;
            }
            int len = this.Character.Waypoints.Count;
            do
            {
                this.activeWaypoint = (this.activeWaypoint + 1) % len;
                result = this.Character.Waypoints[this.activeWaypoint];
            }
            while (result.Position.Distance2D(this.Character.Coordinates().coordinate) < 0.2f);
            return result;
        }

        public void StartMovement()
        {
            this.Character.UpdateMoveType(1); // Magic number: Forward start
        }

        ~NPCController()
        {
            LogUtil.Debug(DebugInfoDetail.Memory, "NPC Controller finished");
            LogUtil.Debug(DebugInfoDetail.Memory, new StackTrace().ToString());
            this.Dispose(false);
        }

        public bool Move(
            int moveType,
            Coordinate newCoordinates,
            CellAO.Messages.Fields.Quaternion heading)
        {
            return false;
        }

        public void Move()
        {
        }

        public void StopFollow()
        {
            this.followIdentity = Identity.None;
            lock (this.followCoordinates)
            {
                this.followCoordinates = new Vector3();
            }
        }

        public void SetKnuBot(BaseKnuBot knubot)
        {
            this.KnuBot = knubot;
        }

        public void AwardXp(IController controller) {
            throw new NotImplementedException();
        }
    }
}