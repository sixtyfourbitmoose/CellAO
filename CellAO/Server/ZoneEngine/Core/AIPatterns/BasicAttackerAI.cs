﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellAO.Core.Combat
{
    using CellAO.Core.Entities;
    using CellAO.ObjectManager;

    using CellAO.Messages.Fields;
    using CellAO.Messages.N3Messages;
    using CellAO.Enums;
    using CellAO.Interfaces;

    public class BasicAttackerAI : IAIPattern
    {
        public ICharacter MostHated
        {
            get
            {
                return Pool.Instance.GetObject<ICharacter>(new Identity() {
                    Type = IdentityType.Dynel,
                    Instance = HateList.ToList().OrderBy(kv => kv.Value).Last().Key
                });

            }
        }
        public ICharacter CurrentlyAttacking
        {
            get
            {
                return Source.CurrentlyAttacking();
            }
        }
        public ICharacter Source { get; set; }
        public BasicAttackerAI(ICharacter character)
        {
            this.Source = character;
            this.HateList = new Dictionary<int, int>();
        }
        public Dictionary<int, int> HateList { get; set; }

        public void AddHate(ICharacter attacker, int amount)
        {
            // Apply hate change
            if (!HateList.ContainsKey(attacker.Identity.Instance))
            {
                HateList.Add(attacker.Identity.Instance, 0);
            }

            Utility.LogUtil.Debug(Utility.DebugInfoDetail.AILogic, String.Format("[{0}] {1} {2} Hate for {3}", Source.Name, (amount < 0 ? '-' : '+'), amount, MostHated.Name));

            HateList[attacker.Identity.Instance] += amount;
            Assess();

        }

        public void Assess()
        {
            if (HateList.Count == 0) // No more attackers 
            {
                Utility.LogUtil.Debug(Utility.DebugInfoDetail.AILogic, String.Format("[{0}] No more attackers", Source.Name));
                // Stop attacking other people
                this.Source.DoStopAttack();
                this.Source.Controller.StartPatrolling(); // Go back to patrolling
                return;
            }

            // Not currently fighting anyone.. start fighting the attacker
            if (CurrentlyAttacking == null)
            {
                Utility.LogUtil.Debug(Utility.DebugInfoDetail.AILogic, String.Format("[{0}] Opening attack {1}", Source.Name, MostHated.Name));

                MostHated.Stats[(int)StatIds.isfightingme].Value = 1;
                MostHated.Stats[(int)StatIds.isfightingme].Changed = true;
                MostHated.SendChangedStats();

                Source.Playfield.Announce(new SpecialAttackWeaponMessage()
                {
                    Identity = Source.Identity,
                    Specials = (new System.Collections.Generic.List<SpecialAttack>()
                            {
                              new SpecialAttack()
                              {
                                  Unknown1 = 121041 ,//client.Controller.Character.GetRightHandWeapon().LowID,
                                  Unknown2 = 121041 ,//client.Controller.Character.GetRightHandWeapon().HighID,
                                  Unknown3 = 1380276018,
                                  Unknown4 = "REW2"
                              },
                              new SpecialAttack()
                              {
                                  Unknown1 = 121038 ,//client.Controller.Character.GetRightHandWeapon().LowID,
                                  Unknown2 = 121038 ,//client.Controller.Character.GetRightHandWeapon().HighID,
                                  Unknown3 = 1380276017 ,
                                  Unknown4 = "REW1"
                              },
                            }).ToArray(),
                    AggDef = 0
                });

                this.Source.BeginAutoAttack(MostHated);

                Source.Playfield.Announce(new AttackMessage()
                {
                    Identity = Source.Identity,
                    Target = MostHated.Identity,
                    Unknown = 0x00,
                    Action = 0x00
                });
            }

            // New most hated target.. must stop fight with old hater and switch to new Most hated
            else if (CurrentlyAttacking != MostHated)
            {
                Utility.LogUtil.Debug(Utility.DebugInfoDetail.AILogic, String.Format("[{0}] Switching target from {1} to {2}", Source.Name, CurrentlyAttacking.Name, MostHated.Name));
                // Stop attacking other people
                this.Source.DoStopAttack();


                // Do we need to delay the attack until they're in range?

                this.Source.BeginAutoAttack(MostHated);

                Source.Playfield.Announce(new AttackMessage()
                {
                    Identity = this.Source.Identity,
                    Target = MostHated.Identity,
                    Unknown = 0x00,
                    Action = 0x00
                });
            }
        }

        public void ClearHate(int instance)
        {
            Utility.LogUtil.Debug(Utility.DebugInfoDetail.AILogic, String.Format("[{0}] Clearing hate for {1}", Source.Name, MostHated.Name));
            HateList.Remove(instance);
            Assess();
        }
        
    }
}
