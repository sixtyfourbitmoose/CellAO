﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellAO.Core.Combat
{
    using CellAO.Core.Entities;

    public interface IAIPattern
    {
        Dictionary<int, int> HateList { get; set; }

        ICharacter Source { get; set; }

        void AddHate(ICharacter character, int amount);
        void ClearHate(int instance);
        void Assess();
    }
}
