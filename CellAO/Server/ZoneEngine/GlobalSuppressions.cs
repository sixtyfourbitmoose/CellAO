﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "It's CellAO Naming Style", Scope = "type", Target = "~T:ZoneEngine.Core.Functions.GameFunctions.teleportproxy")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "It's CellAO Naming Style", Scope = "type", Target = "~T:ZoneEngine.Core.Functions.GameFunctions.teleportproxy2")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0018:Inline variable declaration", Justification = "<Pending>", Scope = "member", Target = "~M:CellAO.Core.Playfields.Playfield.Teleport(CellAO.Core.Entities.Dynel,CellAO.Core.Vector.Coordinate,CellAO.Interfaces.IQuaternion,CellAO.Messages.Fields.Identity)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0018:Inline variable declaration", Justification = "<Pending>", Scope = "member", Target = "~M:ZoneEngine.ChatCommands.AOChatCommand.CheckArgumentHelper(System.Collections.Generic.List{System.Type},System.String[])~System.Boolean")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0018:Inline variable declaration", Justification = "<Pending>", Scope = "member", Target = "~M:ZoneEngine.ChatCommands.ChatCommandGet.ExecuteCommand(CellAO.Core.Entities.ICharacter,CellAO.Messages.Fields.Identity,System.String[])")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0018:Inline variable declaration", Justification = "<Pending>", Scope = "member", Target = "~M:ZoneEngine.ChatCommands.ChatCommandGiveItem.ExecuteCommand(CellAO.Core.Entities.ICharacter,CellAO.Messages.Fields.Identity,System.String[])")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0018:Inline variable declaration", Justification = "<Pending>", Scope = "member", Target = "~M:ZoneEngine.ChatCommands.ChatCommandSet.ExecuteCommand(CellAO.Core.Entities.ICharacter,CellAO.Messages.Fields.Identity,System.String[])")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0018:Inline variable declaration", Justification = "<Pending>", Scope = "member", Target = "~M:ZoneEngine.Core.MessageHandlers.ChatServerInfoMessageHandler.Filler(CellAO.Core.Entities.ICharacter)~CellAO.Core.Components.AbstractMessageHandler{CellAO.Messages.SystemMessages.ChatServerInfoMessage}.MessageDataFiller")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "member", Target = "~M:ZoneEngine.Core.MessageHandlers.ContainerAddItemMessageHandler.getAction(CellAO.Core.Inventory.IInventoryPage,CellAO.Core.Items.IItem)~CellAO.Core.Actions.AOAction")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "member", Target = "~P:ZoneEngine.Core.Playfields.PlayfieldInfo.id")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "type", Target = "~T:ZoneEngine.ChatCommands.tpout")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "type", Target = "~T:ZoneEngine.Core.Functions.GameFunctions.backmesh")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "type", Target = "~T:ZoneEngine.Core.Functions.GameFunctions.exitproxyplayfield")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "type", Target = "~T:ZoneEngine.Core.Functions.GameFunctions.hit")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "type", Target = "~T:ZoneEngine.Core.Functions.GameFunctions.lineteleport")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "type", Target = "~T:ZoneEngine.Core.Functions.GameFunctions.openbank")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "type", Target = "~T:ZoneEngine.Core.Functions.GameFunctions.shophash")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "type", Target = "~T:ZoneEngine.Core.Functions.GameFunctions.systemtext")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "type", Target = "~T:ZoneEngine.Core.Functions.GameFunctions.teleport")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "type", Target = "~T:ZoneEngine.Core.Functions.GameFunctions.uploadnano")]
