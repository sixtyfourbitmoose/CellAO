﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace ZoneEngine
{
    #region Usings ...

    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;

    using CellAO.Communication.ISComV2Client;
    using CellAO.Communication.Messages;
    using CellAO.Core.Actions;
    using CellAO.Core.Events;
    using CellAO.Core.Items;
    using CellAO.Core.Nanos;
    using CellAO.Database;

    using locales;

    using NBug;
    using NBug.Properties;

    using NLog;

    using Utility;

    using ZoneEngine.Core;
    using ZoneEngine.Core.Functions;
    using ZoneEngine.Core.Playfields;
    using ZoneEngine.Script;

    #endregion

    /// <summary>
    /// Program Class for ZoneEngine
    /// </summary>
    internal class Program
    {
        #region Static Fields

        /// <summary>
        /// </summary>
        public static ISComV2Client ISComClient;

        /// <summary>
        /// </summary>
        public static ZoneServer zoneServer;

        /// <summary>
        /// </summary>
        private static readonly ServerConsoleCommands consoleCommands = new ServerConsoleCommands();

        /// <summary>
        /// </summary>
        private static bool exited = false;

        #endregion

        #region Methods

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        private static bool CheckZoneServerCreation()
        {
            try
            {
                zoneServer = new ZoneServer();
            }
            catch (Exception e)
            {
                LogUtil.ErrorException(e);
                return false;
            }

            return true;
        }

        /// <summary>
        /// </summary>
        /// <param name="args">
        /// </param>
        private static void CommandLoop(string[] args)
        {
            bool processedargs = false;
            Console.WriteLine(locales.ZoneEngineConsoleCommands);

            while (!exited)
            {
                if (!processedargs)
                {
                    if (args.Length == 1)
                    {
                        if (args[0].ToLower() == "/autostart")
                        {
                            Console.WriteLine(locales.ServerConsoleAutostart);
                            ScriptCompiler.Instance.Compile(false);
                            StartTheServer();
                        }
                    }

                    processedargs = true;
                }

                string consoleCommand = Console.ReadLine();

                if (consoleCommand != null)
                {
                    if (!consoleCommands.Execute(consoleCommand))
                    {
                        ShowCommandHelp();
                    }
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        private static void ConsoleCancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            if (zoneServer != null)
            {
                exited = true;
                ISComClient.ShutDown();
                zoneServer.DisconnectAllClients();
                LogUtil.Debug(DebugInfoDetail.Engine, "Shutting down ZoneEngine hard");
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="messageobject">
        /// </param>
        private static void ISComClientOnReceiveData(object sender, DynamicMessage messageobject)
        {
            zoneServer.ProcessISComMessage(messageobject);
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        private static bool ISComInitialization()
        {
            int port;
            IPAddress chatEngineIp;
            try
            {
                ISComClient = new ISComV2Client();
                string chatip = Config.Current.ChatIP;
                chatEngineIp = IPAddress.Parse(chatip);
                port = Config.Current.CommPort;
            }
            catch (Exception e)
            {
                LogUtil.ErrorException(e);
                return false;
            }

            try
            {
                ISComClient.OnReceiveData += ISComClientOnReceiveData;
                ISComClient.Connect(chatEngineIp, port);
            }
            catch (Exception e)
            {
                LogUtil.ErrorException(e);
                return true;
            }

            return true;
        }

        /// <summary>
        /// Initializing methods go here
        /// </summary>
        /// <returns>
        /// true if ok
        /// </returns>
        private static bool Initialize()
        {
            Console.WriteLine();

            Colouring.Push(ConsoleColor.Green);
            if (!InitializeLogAndBug())
            {
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ErrorInitializingNLogNBug);
                return false;
            }


            Colouring.Push(ConsoleColor.Green);
            if (!InitializeConsoleCommands())
            {
                return false;
            }
            Colouring.Pop();

            Colouring.Push(ConsoleColor.Green);
            if (!CheckZoneServerCreation())
            {
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ErrorCreatingZoneServerInstance);
                return false;
            }

            Colouring.Push(ConsoleColor.Green);
            if (!ISComInitialization())
            {
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ErrorInitializingISCom);
                return false;
            }
            Colouring.Pop();

            Colouring.Push(ConsoleColor.Green);
            if (!InizializeTCPIP())
            {
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ErrorTCPIPSetup);
                return false;
            }
            Colouring.Pop();

            Colouring.Push(ConsoleColor.Green);
            if (!InitializeDatabase())
            {
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ErrorInitializingDatabase);
                return false;
            }
            Colouring.Pop();

            Colouring.Push(ConsoleColor.Green);
            if (!InitializeGameFunctions())
            {
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ErrorInitializingGamefunctions);
                return false;
            }
            Colouring.Pop();

            Colouring.Push(ConsoleColor.Green);
            if (!LoadItemsAndNanos())
            {
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ErrorLoadingItemsNanos);
                return false;
            }
            Colouring.Pop();

            Colouring.Push(ConsoleColor.Green);
            if (!LoadTradeSkills())
            {
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine("Error loading trade skills");
                return false;
            }
            Colouring.Pop();


            return true;
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        private static bool InitializeConsoleCommands()
        {
            consoleCommands.Engine = "Zone";

            consoleCommands.AddEntry("start", StartServer);
            consoleCommands.AddEntry("startm", StartServerMultipleScriptDlls);
            consoleCommands.AddEntry("running", IsServerRunning);
            consoleCommands.AddEntry("ping", PingChatServer);

            consoleCommands.AddEntry("stop", StopServer);

            consoleCommands.AddEntry("exit", ShutDownServer);
            consoleCommands.AddEntry("quit", ShutDownServer);

            consoleCommands.AddEntry("db", DoSqlDatabaseCmd); // good name, isnt' it ?

            consoleCommands.AddEntry("online", ShowOnlineCharacters);
            consoleCommands.AddEntry("ls", ListAvailableScripts);

            consoleCommands.AddEntry("debug", SetDebug);

            return true;
        }

        private static bool InitializeDatabase()
        {
            bool success = false;
            try
            {
                Colouring.Push(ConsoleColor.Cyan);
                Console.Write("Initializing database...");
                success = CellAO.Database.Misc.CheckDatabase(true, true, false);
                Console.WriteLine(success ? "OK" : " NOT OK :( ");

            }
            catch (Exception e)
            {
                LogUtil.ErrorException(e);

                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ErrorInitializingDatabase);
                Console.WriteLine(e.Message);
                Colouring.Pop();

            }

            return success;

        }

        private static void DoSqlDatabaseCmd(string[] obj)
        {
            bool completeCheck = true, // by default will go through all entities/tables
                createIfNotExists = false, // by default will not create tables even if they aren't there
                overwriteIfExists = false; // by default will not try to overwrite an existing database

            if (obj.Contains("--help"))
            {
                Colouring.Push(ConsoleColor.Cyan);
                Console.WriteLine("  Syntax: db [--create] [--upgrade] [--refill]");
                Console.WriteLine("    By default, checks the database structure (checks if all DBEntities have a table)");
                Console.WriteLine("    --create    Creates tables if they don't exist yet. Existing tables will be untouched.");
                Console.WriteLine("    --upgrade   Creates missing tables and ovewrite existing tables with latest version. ALL DATA will be lost.");
                Console.WriteLine("    --fill   Creates missing tables and ovewrite existing tables with latest version. ALL DATA will be lost.");
                Colouring.Pop();
                return;
            }

            if (obj.Contains("--create"))
            {
                createIfNotExists = true;

                Colouring.Push(ConsoleColor.Cyan);
                Console.Write("Drop existing tables ? (Y/N) ");
                Colouring.Pop();

                if (Console.ReadLine().ToLower() == "y")
                {
                    overwriteIfExists = true;
                }
            }

            if (obj.Contains("--upgrade"))
            {
                createIfNotExists = true;

                Colouring.Push(ConsoleColor.Cyan);
                Console.Write("Are you sure you want to drop existing tables and their content ? (Y/N) ");
                Colouring.Pop();

                if (Console.ReadLine().ToLower() == "y")
                {
                    overwriteIfExists = true;
                }
            }

            if (obj.Contains("--fill"))
            {
                Colouring.Push(ConsoleColor.Cyan);
                Console.Write("Do you want to empty the tables before importing ? (Y/N) ");
                Colouring.Pop();

                bool truncate = false;
                if (Console.ReadLine().ToLower() == "y")
                {
                    truncate = true;
                }
                CellAO.Database.Misc.ImportData(truncate);

                Console.WriteLine("Done.");
                return;
            }

            if (CellAO.Database.Misc.CheckDatabase(completeCheck, createIfNotExists, overwriteIfExists))
            {
                Colouring.Push(ConsoleColor.Green);
                Console.WriteLine("Done.");
                Colouring.Pop();
            }
        }

        private static void SetDebug(string[] obj)
        {
            if (obj.Length == 1)
            {
                LogUtil.Toggle("");
            }
            else
            {
                for (int i = 1; i < obj.Length; i++)
                {
                    LogUtil.Toggle(obj[i]);
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        private static bool InitializeGameFunctions()
        {
            bool success = true;
            try
            {
                Colouring.Push(ConsoleColor.Green);
                Console.Write($"Loading game functions...");
                var loaded = FunctionCollection.Instance.NumberofRegisteredFunctions();
                Console.WriteLine($" {loaded} loaded.");
            }
            catch (Exception e)
            {
                LogUtil.ErrorException(e);

                success = false;
            }
            finally
            {
                Colouring.Pop();
            }

            return success;
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        private static bool InitializeLogAndBug()
        {
            try
            {
                // Setup and enable NLog logging to file
                LogUtil.SetupConsoleLogging(LogLevel.Debug);
                LogUtil.SetupFileLogging("${basedir}/ZoneEngineLog.txt", LogLevel.Trace);

                // NBug initialization
                SettingsOverride.LoadCustomSettings("NBug.ZoneEngine.config");
                Settings.WriteLogToDisk = true;
                AppDomain.CurrentDomain.UnhandledException += Handler.UnhandledException;
                TaskScheduler.UnobservedTaskException += Handler.UnobservedTaskException;
            }
            catch (Exception e)
            {
                LogUtil.ErrorException(e);

                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ErrorInitializingNLogNBug);
                Console.WriteLine(e.Message);
                Colouring.Pop();
                return false;
            }

            return true;
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        private static bool InizializeTCPIP()
        {
            int Port = Convert.ToInt32(Config.Current.ZonePort);
            try
            {
                if (Config.Current.ListenIP == "0.0.0.0")
                {
                    zoneServer.TcpEndPoint = new IPEndPoint(IPAddress.Any, Port);
                }
                else
                {
                    zoneServer.TcpEndPoint = new IPEndPoint(IPAddress.Parse(Config.Current.ListenIP), Port);
                }

                zoneServer.MaximumPendingConnections = 100;
            }
            catch (Exception e)
            {
                LogUtil.ErrorException(e);

                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ErrorIPAddressParseFailed);
                Console.Write(e.Message);
                Colouring.Pop();
                return false;
            }

            return true;
        }

        /// <summary>
        /// </summary>
        /// <param name="parts">
        /// </param>
        private static void IsServerRunning(string[] parts)
        {
            Colouring.Push(ConsoleColor.White);
            if (zoneServer.IsRunning)
            {
                Console.WriteLine(locales.ServerConsoleServerIsRunning);
            }
            else
            {
                Console.WriteLine(locales.ServerConsoleServerIsNotRunning);
            }

            Colouring.Pop();
        }

        /// <summary>
        /// </summary>
        /// <param name="parts">
        /// </param>
        private static void ListAvailableScripts(string[] parts)
        {
            // list all available scripts, dont remove it since it does what it should
            Colouring.Push(ConsoleColor.White);
            Console.WriteLine(locales.ServerConsoleAvailableScripts + ":");

            string[] files = Directory.GetFiles(
                "Scripts" + Path.DirectorySeparatorChar,
                "*.cs",
                SearchOption.AllDirectories);
            if (files.Length == 0)
            {
                Console.WriteLine(locales.ServerConsoleNoScriptsFound);
                return;
            }

            Colouring.Push(ConsoleColor.Green);
            foreach (string s in files)
            {
                Console.WriteLine(s);
            }

            Colouring.Pop();
        }

        /// <summary>
        /// Load items and Nanos into static lists
        /// </summary>
        /// <returns>
        /// true if ok
        /// </returns>
        private static bool LoadItemsAndNanos()
        {
            Colouring.Push(ConsoleColor.Green);
            try
            {
                Console.Write($"Loading items... ");
                var count = ItemLoader.CacheAllItems();
                Console.WriteLine(locales.ItemLoaderLoadedItems, count);
            }
            catch (Exception e)
            {
                LogUtil.ErrorException(e);

                Colouring.Pop();
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ErrorReadingItemsFile);
                Console.WriteLine(e.Message);
                Colouring.Pop();
                return false;
            }

            Colouring.Pop();

            Colouring.Push(ConsoleColor.Green);
            try
            {
                Console.Write("Loading Nanos... ");
                var count = NanoLoader.CacheAllNanos();
                Console.WriteLine(locales.NanoLoaderLoadedNanos, count);
                Console.WriteLine();
            }
            catch (Exception e)
            {
                LogUtil.ErrorException(e);

                Colouring.Pop();
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ErrorReadingNanosFile);
                Console.WriteLine(e.Message);
                Colouring.Pop();
                return false;
            }

            Colouring.Pop();

            Colouring.Push(ConsoleColor.Green);
            try
            {
                Console.Write("Loading Playfields... ");
                var count = PlayfieldLoader.CacheAllPlayfieldData();
                Console.WriteLine(" {0} loaded.", count);
                Console.WriteLine();
            }
            catch (Exception e)
            {
                LogUtil.ErrorException(e);

                Colouring.Pop();
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine("Error reading playfields.dat file");
                Console.WriteLine(e.Message);
                Colouring.Pop();
                return false;
            }

            Colouring.Pop();

            return true;
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        private static bool LoadTradeSkills()
        {
            try
            {
                Console.Write("Loading Tradeskills... "); // from database, actually

                Console.WriteLine($" {TradeSkill.Instance.ItemNames.Count} item names loaded");
                Console.WriteLine();
            }
            catch (Exception e)
            {
                LogUtil.ErrorException(e);

                return false;
            }

            return true;
        }

        /// <summary>
        /// Entry point
        /// </summary>
        /// <param name="args">
        /// Command line parameters
        /// </param>
        private static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.CancelKeyPress += ConsoleCancelKeyPress;

            OnScreenBanner.PrintCellAOBanner(ConsoleColor.Green);

            Console.WriteLine();
            Console.WriteLine(locales.ServerConsoleMainText, DateTime.Now.Year);

            if (!Initialize())
            {
                Console.WriteLine(locales.ErrorInitializingEngine);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
            }
            else
            {
#if DEBUG
                StartTheServer();
#endif
                CommandLoop(args);
            }

            // NLog<->Mono lockup fix
            LogManager.Configuration = null;
        }

        /// <summary>
        /// </summary>
        /// <param name="parts">
        /// </param>
        private static void PingChatServer(string[] parts)
        {
            // ChatCom.Server.Ping();
            Console.WriteLine("Ping is disabled till we can do it");
        }

        /// <summary>
        /// </summary>
        private static void ShowCommandHelp()
        {
            Colouring.Push(ConsoleColor.White);
            Console.WriteLine(locales.ServerConsoleAvailableCommands);
            Console.WriteLine("---------------------------");
            Console.WriteLine(consoleCommands.HelpAll());
            Console.WriteLine("---------------------------");
            Console.WriteLine();
            Colouring.Pop();
        }

        /// <summary>
        /// </summary>
        /// <param name="parts">
        /// </param>
        private static void ShowOnlineCharacters(string[] parts)
        {
            if (zoneServer.IsRunning)
            {
                Colouring.Push(ConsoleColor.White);

                // TODO: Check all clients inside playfields
                lock (zoneServer.Clients)
                {
                    foreach (ZoneClient c in zoneServer.Clients)
                    {
                        Console.WriteLine(
                            "Character " + c.Controller.Character.Name + " online in PF "
                            + c.Controller.Character.Playfield.Identity.Instance);
                    }
                }

                Colouring.Pop();
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="parts">
        /// </param>
        private static void ShutDownServer(string[] parts)
        {
            if (zoneServer.IsRunning)
            {
                zoneServer.Stop();
            }

            ISComClient.ShutDown();
            exited = true;
        }

        /// <summary>
        /// </summary>
        /// <param name="parts">
        /// </param>
        private static void StartServer(string[] parts)
        {
            if (zoneServer.IsRunning)
            {
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ServerConsoleServerIsRunning);
                Colouring.Pop();
            }
            else
            {
                // TODO: Add Sql Check.
                ScriptCompiler.Instance.Compile(false);
                StartTheServer();
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="parts">
        /// </param>
        private static void StartServerMultipleScriptDlls(string[] parts)
        {
            // Multiple dll compile
            if (zoneServer.IsRunning)
            {
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ServerConsoleServerIsRunning);
                Colouring.Pop();
            }
            else
            {
                // TODO: Add Sql Check.
                ScriptCompiler.Instance.Compile(true);
                StartTheServer();
            }
        }

        /// <summary>
        /// </summary>
        private static void StartTheServer()
        {
            // TODO: Read playfield data, check which playfields have to be created, and create them
            // TODO: Cache neccessary Spawns and Mobs
            // TODO: Cache neccessary Doors 
            // TODO: Cache neccessary statels
            // TODO: Cache Vendors

            // Console.WriteLine(Core.Playfields.Playfields.Instance.playfields[0].name);

            ScriptCompiler.Instance.Compile(true);
            Console.WriteLine(ScriptCompiler.Instance.AddScriptMembers() + " chat commands loaded");
            zoneServer.Start(true, false);

            // Uncomment out the following lines to dump the dats as json
            //System.IO.File.WriteAllText("nanos.json", Newtonsoft.Json.JsonConvert.SerializeObject(NanoLoader.NanoList));
            //System.IO.File.WriteAllText("items.json", Newtonsoft.Json.JsonConvert.SerializeObject(ItemLoader.ItemList));
            //System.IO.File.WriteAllText("playfields.json", Newtonsoft.Json.JsonConvert.SerializeObject(PlayfieldLoader.PFData));
        }

        /// <summary>
        /// </summary>
        /// <param name="parts">
        /// </param>
        private static void StopServer(string[] parts)
        {
            if (!zoneServer.IsRunning)
            {
                Colouring.Push(ConsoleColor.Red);
                Console.WriteLine(locales.ServerConsoleServerIsNotRunning);
                Colouring.Pop();
            }
            else
            {
                zoneServer.Stop();
            }
        }

        #endregion
    }
}