﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateCharacterMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the CreateCharacterMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.SystemMessages
{
    using CellAO.Enums;
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces.Attributes;
    using CellAO.Messages.Fields;

    [AoContract((int)SystemMessageType.CreateCharacter)]
    public class CreateCharacterMessage : SystemMessage
    {
        #region Constructors and Destructors

        public CreateCharacterMessage()
        {
            this.SystemMessageType = SystemMessageType.CreateCharacter;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0, IsFixedSize = true, FixedSizeLength = 49)]
        public byte[] Unknown1 { get; set; }

        [MessageField(1, SerializeSize = ArraySizeType.Int32)]
        public string Name { get; set; }

        [MessageField(2)]
        public Breed Breed { get; set; }

        [MessageField(3)]
        public Gender Gender { get; set; }

        [MessageField(4)]
        public Profession Profession { get; set; }

        [MessageField(5)]
        public int Level { get; set; }

        [MessageField(6, SerializeSize = ArraySizeType.Int32)]
        public string AreaName { get; set; }

        [MessageField(7)]
        public int Unknown2 { get; set; }

        [MessageField(8)]
        public int Unknown3 { get; set; }

        [MessageField(9)]
        public int HeadMesh { get; set; }

        [MessageField(10)]
        public int MonsterScale { get; set; }

        [MessageField(11)]
        public Fatness Fatness { get; set; }

        [MessageField(12)]
        public StarterArea StarterArea { get; set; }

        #endregion
    }
}