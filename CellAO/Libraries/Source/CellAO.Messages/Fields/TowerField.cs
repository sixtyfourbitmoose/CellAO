﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TowerField.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the TowerField type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.Fields
{
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    public class TowerField
    {
        #region AoMember Properties

        [MessageField(0)]
        public int Unknown1 { get; set; }

        [MessageField(1)]
        public Identity Identity { get; set; }

        [MessageField(2, SerializeSize = ArraySizeType.Int16)]
        public string Name { get; set; }

        [MessageField(3)]
        public int Unknown2 { get; set; }

        [MessageField(4)]
        public int Unknown3 { get; set; }

        #endregion
    }
}