﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    public class QuestIdentity
    {
        [MessageField(0)]
        public Identity Unknown1 { get; set; }
        [MessageField(1)]
        public int Unknown2 { get; set; }
    }
}
