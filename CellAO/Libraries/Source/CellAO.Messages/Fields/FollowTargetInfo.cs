﻿namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;


    /// <summary>
    /// </summary>
    public class FollowTargetInfo : FollowInfo
    {
        private byte followInfoType = 2;

        /// <summary>
        /// </summary>
        [MessageField(0)]
        public byte FollowInfoType
        {
            get
            {
                return this.followInfoType;
            }
            set
            {
                this.followInfoType = value;
            }
        }

        public FollowTargetInfo()
        {
            this.MoveType = 0;
        }

        /// <summary>
        /// DataLength is 0 for FollowTargetInfo
        /// </summary>
        [MessageField(1)]
        public byte MoveType { get; set; }

        /// <summary>
        /// </summary>
        [MessageField(2)]
        public Identity Target { get; set; }

        [MessageField(3)]

        public byte Dummy { get; set; }
        /// <summary>
        /// 0x20000000
        /// </summary>
        [MessageField(4)]
        public int Dummy1 { get; set; }

        /// <summary>
        /// </summary>
        [MessageField(5)]
        public float X { get; set; }

        /// <summary>
        /// </summary>
        [MessageField(6)]
        public float Y { get; set; }

        /// <summary>
        /// </summary>
        [MessageField(7)]
        public float Z { get; set; }
    }
}