﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    public class TowerProxyBase
    {
        [MessageField(1)]
        public Identity TowerFieldIdentity { get; set; }
        [MessageField(2)]
        public Identity OwnerIdentity { get; set; }
        [MessageField(3)]
        public Vector3 Coordinates { get; set; }
        [MessageField(4)]
        public int Unknown1 { get; set; }
        [MessageField(5)]
        public int Unknown2 { get; set; }
        [MessageField(6)]
        public int Unknown3 { get; set; }
        [MessageField(7)]
        public Single Unknown4 { get; set; }
        [MessageField(8)]
        public int Unknown5 { get; set; }
    }
}
