﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.Fields
{
    using System.Dynamic;

    using CellAO.Interfaces.Attributes;

    public class QuestFaction
    {
        [MessageField(0)]
        public int Unknown1 { get; set; }
        [MessageField(1)]
        public int Unknown2 { get; set; }
    }
}
