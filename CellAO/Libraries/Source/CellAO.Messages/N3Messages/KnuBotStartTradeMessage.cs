﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KnuBotStartTradeMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the KnuBotStartTradeMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    [AoContract((int)N3MessageType.KnuBotStartTrade)]
    public class KnuBotStartTradeMessage : N3Message
    {
        #region Constructors and Destructors

        public KnuBotStartTradeMessage()
        {
            this.N3MessageType = N3MessageType.KnuBotStartTrade;
            this.Identity = new Identity();
            this.Target = new Identity();
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public short Unknown1 { get; set; }

        [MessageField(1)]
        public Identity Target { get; set; }

        [MessageField(2)]
        public int NumberOfItemSlotsInTradeWindow { get; set; }

        [MessageField(3, SerializeSize = ArraySizeType.Int32)]
        public string Message { get; set; }

        #endregion
    }
}