﻿namespace CellAO.Messages.N3Messages
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    [AoContract((int)N3MessageType.GridSelected)]
    class GridSelectedMessage : N3Message
    {
        public GridSelectedMessage()
        {
            N3MessageType = N3MessageType.GridSelected;
        }
    }
}