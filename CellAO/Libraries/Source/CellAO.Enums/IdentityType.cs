﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Enums
{
    public enum IdentityType
    {
        FightContext = 0x9999999,
        
        InstancedItem = 0x9000001,

        None = 0, 

        WeaponPage = 0x00000065, 

        ArmorPage = 0x00000066, 

        ImplantPage = 0x00000067, 

        Inventory = 0x00000068, 

        Bank = 0x00000069, 

        Backpack = 0x0000006B, 

        // On Trade this will be the bag for the Character's items to sell
        KnuBotTradeWindow = 0x0000006C, 

        OverflowWindow = 0x0000006E, 

        // On Trade this will be the bag for the Items to buy
        TradeWindow = 0x0000006F, 

        SocialPage = 0x00000073, 

        ShopInventory = 0x00000767, 

        PlayerShopInventory = 0x00000790, 

        Playfield2 = 0x00009C50, 

        Dynel = 0x0000C350,

        CityController = 0x0000C418,

        Terminal = 0x0000C73D,

        Door = 0x0000C748,

        WeaponInstance = 0x0000C74A, 

        VendingMachine = 0x0000C75B, 

        TempBag = 0x0000C767, 

        Corpse = 0x0000C76A,

        MailTerminal = 0x0000C773,
        
        Playfield1 = 0x0000C79C, 

        Playfield = 0x0000C79D, 

        NanoProgram = 0x0000CF1B, 

        GfxEffect = 0x0000CF26,

        MissionEntrance = 0x0000DAC6,

        MissionTerminal = 0x0000DCA1,

        TeamWindow = 0x0000DEA9, 

        Organization = 0x0000DEAA, 

        IncomingTradeWindow = 0x0000DEAD, 

        Playfield3 = 0x000186A1,

        UnknownRDB1 = 0x000F424F
    }
}