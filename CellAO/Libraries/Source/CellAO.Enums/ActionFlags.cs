﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellAO.Enums
{
    [Flags]
    public enum ActionFlags
    {
        None = 0,
        Movement = 4
    }
}
