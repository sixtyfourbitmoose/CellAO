﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Database.Entities
{

    using System.Collections.Generic;

    /// <summary>
    /// Data object for Character DAO
    /// </summary>
    [Table("Characters", autoIncrementFrom: 18)]
    public class DBCharacter : DBEntity
    {

        [PrimaryKey]
        [Column(nullable: false, autoIncrement: true)]
        public override int Id { get; set; }

        /// <summary>
        /// Username of the character
        /// </summary>
        [Column(nullable: false, foreignKey: true)]
        public string Username { get; set; }

        /// <summary>
        /// Name of the character
        /// </summary>
        [Column(nullable: false)]
        public string Name { get; set; }

        /// <summary>
        /// First name of the character
        /// </summary>
        [Column(nullable: false)]
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of the character
        /// </summary>
        [Column(nullable: false)]
        public string LastName { get; set; }

        /// <summary>
        /// Texture #0
        /// </summary>
        [Column(nullable: false)]
        public int Textures0 { get; set; }

        /// <summary>
        /// Texture #1
        /// </summary>
        [Column(nullable: false)]
        public int Textures1 { get; set; }

        /// <summary>
        /// Texture #2
        /// </summary>
        [Column(nullable: false)]
        public int Textures2 { get; set; }

        /// <summary>
        /// Texture #3
        /// </summary>
        [Column(nullable: false)]
        public int Textures3 { get; set; }

        /// <summary>
        /// Texture #4
        /// </summary>
        [Column(nullable: false)]
        public int Textures4 { get; set; }

        /// <summary>
        /// Playfield
        /// </summary>
        [Column(nullable: false)]
        public int Playfield { get; set; }

        /// <summary>
        /// Coordinates (X)
        /// </summary>
        [Column(nullable: false)]
        public float X { get; set; }

        /// <summary>
        /// Coordinates (Y)
        /// </summary>
        [Column(nullable: false)]
        public float Y { get; set; }

        /// <summary>
        /// Coordinates (Z)
        /// </summary>
        [Column(nullable: false)]
        public float Z { get; set; }
        
        /// <summary>
        /// Heading (X)
        /// </summary>
        [Column]
        public float HeadingX { get; set; }

        /// <summary>
        /// Heading (Y)
        /// </summary>
        [Column(nullable: false)]
        public float HeadingY { get; set; }

        /// <summary>
        /// Heading (Z)
        /// </summary>
        [Column(nullable: false)]
        public float HeadingZ { get; set; }

        /// <summary>
        /// Heading (W)
        /// </summary>
        [Column(nullable: false)]
        public float HeadingW { get; set; }

        /// <summary>
        /// 0 by default (int)
        /// </summary>
        [Column(DBColumnType.SmallInt, length: 6)]
        public short Online { get; set; }

        /// <summary>
        /// CSV list of players ID that are buddies of meeee
        /// </summary>
        [Column(length: 500, nullable: true)]
        public string BuddyList { get; set; }   

        #region Buddies intelligence - cos my buddies are intelligent otherwise they wouldnt be my buddies :) - Yeah but we should keep DB entities free from logic, and leave that to managers.

        public List<int> GetBuddiesIds()
        {
            List<int> buddiesIds = new List<int>();
            foreach (string strId in this.BuddyList.Split(','))
            {
                buddiesIds.Add(int.Parse(strId));
            }
            return buddiesIds;
        }

        public void AddBuddy(int buddyId)
        {
            List<string> buddies = new List<string>();
            buddies.AddRange(this.BuddyList.Split(','));
            if (!buddies.Contains(buddyId.ToString()))
            {
                buddies.Add(buddyId.ToString());
            }

            this.BuddyList = string.Join(",", buddies);
        }

        public void RemoveBuddy(int buddyId)
        {
            List<string> buddies = new List<string>();
            buddies.AddRange(this.BuddyList.Split(','));
            if (buddies.Contains(buddyId.ToString()))
            {
                buddies.Remove(buddyId.ToString());
            }

            this.BuddyList = string.Join(",", buddies);
        }

        #endregion
    }
}