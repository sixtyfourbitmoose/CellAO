﻿using CellAO.Core.Exceptions;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace CellAO.Database.Entities
{
    /// <summary>
    /// The base class for all database entities
    /// </summary>
    public class DBEntity: IDBEntity
    {

        [PrimaryKey]
        [Column(length: 32, nullable: false, autoIncrement: true)]
        public virtual int Id { get; set; }

        public DBEntity() { }


    }
}
