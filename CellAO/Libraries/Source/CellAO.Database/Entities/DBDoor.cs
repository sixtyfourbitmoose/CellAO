﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Database.Entities
{
    #region Usings ...

    using System.Data.Linq;

    using CellAO.Database.Dao;

    #endregion

    /// <summary>
    /// </summary>
    [Table("Doors")]
    public class DBDoor : DBEntity
    {
        [PrimaryKey]
        [Column(length: 32, nullable: false)]
        public override int Id { get; set; }

        [Column(nullable: false)]
        public float X { get; set; }

        [Column(nullable: false)]
        public float Y { get; set; }

        [Column(nullable: false)]
        public float Z { get; set; }

        [Column(nullable: false, length: 10)]
        public uint Playfield { get; set; }

        [Column(nullable: false, length: 10)]
        public uint ToPlayfield { get; set; }

		[Column(nullable: false, length: 11)]
        public int ToId { get; set; }

		[Column(DBColumnType.SmallInt, length: 1, nullable: false, defaultValue: 0)]
        public short Proxy { get; set; }

		[Column(nullable: false)]
        public float HX { get; set; }

        [Column(nullable: false)]
        public float HY { get; set; }

        [Column(nullable: false)]
        public float HZ { get; set; }

        [Column(nullable: false)]
        public float HW { get; set; }

    }
}
