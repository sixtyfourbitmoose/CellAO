﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Database
{
    #region Usings ...

    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using Konsole;
    using System.Collections.Concurrent;
    using Dapper;

    using Utility;

    using CellAO.Enums;
    using System.Reflection;
    using CellAO.Database.Entities;
    using CellAO.Core.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections;
    using System.Globalization;
    using System.Data.Linq;

    #endregion

    /// <summary>
    /// </summary>
    public static class Misc
    {

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        public static bool CheckDatabase(bool completeCheck = true, bool createIfNotExists = false, bool overwriteIfExists = false, bool interactive = false)
        {
            bool exists = false;
            try
            {
                var dbEntityTypes = System.Reflection.Assembly.GetExecutingAssembly().GetTypes().Where(t => t.Namespace == "CellAO.Database.Entities");

                foreach (var dbEntityType in dbEntityTypes)
                {
                    if ((dbEntityType.BaseType == null) || (!dbEntityType.BaseType.Name.Equals("DBEntity")))
                        continue;

                    // find and instanciate the DAO for this entity type
                    var daoType = GetDaoForDBEntity(dbEntityType);
                    var dao = Activator.CreateInstance(daoType);

                    // Check if the database exists for each DBEntity 
                    var tableAtts = dbEntityType.GetCustomAttributes<TableAttribute>();
                    if ((tableAtts == null) || (tableAtts.Count() == 0))
                        throw new DataBaseException($"DBEntity '{dbEntityType.Name}' is missing the 'Table[]' class attribute.");

                    var tableExistsMethod = daoType.GetMethods().Single(m => m.Name == "TableExists");
                    exists = (bool)(tableExistsMethod.Invoke(dao, null));
                    if (exists)
                    {
                        if (interactive)
                        {
                            Colouring.Push(ConsoleColor.Red);
                            Console.WriteLine($"Table '{dbEntityType.Name}' already exists in database. Overwrite ? (Y/N) "); // TODO Attempt an upgrade instead of overwrite
                            Colouring.Pop();
                            overwriteIfExists = (Console.ReadLine().ToLower() == "y");
                        }

                        if (!overwriteIfExists)
                            continue;


                        // Check if table has Data and if so, reconfirm the deletion 
                        var hasRecordsMethod = daoType.GetMethods().SingleOrDefault(m => m.Name == "HasRecords");
                        var hasRecords = (bool)(hasRecordsMethod.Invoke(dao, null));
                        if (hasRecords)
                        {
                            if (interactive)
                            {
                                Colouring.Push(ConsoleColor.Red);
                                Console.WriteLine($" /!\\ Table '{dbEntityType.Name}' has some data in it !! You will lose all the data. Do you really want to empty it all ?  (Y/N) ");
                                Colouring.Pop();

                                if (Console.ReadLine().ToLower() == "n")
                                {
                                    continue; // don't delete the existing table
                                }
                            } // otherwise, we JUST DO IT
                        }

                        var deleteTableMethod = daoType.GetMethods().Single(m => m.Name == "DeleteTable");
                        deleteTableMethod.Invoke(dao, new object[] { true });

                        exists = false;

                    }


                    // either does not exists OR ok to overwrite existing

                    if (!exists)
                    {
                        if (createIfNotExists)
                        {
                            Console.WriteLine($"Creating table '{dbEntityType.Name}'...");

                            var createTableMethod = daoType.GetMethods().Single(m => m.Name == "CreateTable");
                            createTableMethod.Invoke(dao, new object[] { true });

                            exists = true;
                        }
                        else
                        {

                            Colouring.Push(ConsoleColor.Red);
                            Console.WriteLine($"The table '{dbEntityType.Name}' does not exist. Please run the ZoneEngine to create all required databases.");
                            Colouring.Pop();

                            if (!completeCheck)
                                return false;
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Utility.LogUtil.ErrorException(e);
                return false;
            }

            return exists;
        }

        public static void ImportData(bool truncate = false)
        {
            var dbEntityTypes = System.Reflection.Assembly.GetExecutingAssembly().GetTypes().Where(t => t.Namespace == "CellAO.Database.Entities");

            var tasks = new List<Task>();
            var bars = new ConcurrentBag<ProgressBar>();

            foreach (var dbEntityType in dbEntityTypes)
            {
                if ((dbEntityType.BaseType == null) || (!dbEntityType.BaseType.Name.Equals("DBEntity")))
                    continue;

                var t = new Task(() =>
                {
                    var maxTicks = 10;
                    // find the DAO for this entity type
                    var daoType = GetDaoForDBEntity(dbEntityType);

                    var dao = Activator.CreateInstance(daoType);
                    var columns = (IEnumerable<ColumnAttribute>)(daoType.InvokeMember("GetColumns", BindingFlags.InvokeMethod, null, dao, null));

                    var tableAtts = dbEntityType.GetCustomAttributes<TableAttribute>();
                    if ((tableAtts == null) || (tableAtts.Count() == 0))
                        return; // ignore, missing propert class attribute on entity class

                    var tableName = ((TableAttribute)tableAtts.First()).Name;
                    var fileName = Path.Combine(Directory.GetCurrentDirectory(), "Data", dbEntityType.Name + ".json");

                    try
                    {

                        // if there is a file named as per the entity class then try to import it
                        if (File.Exists(fileName))
                        {


                            if (truncate)
                            {
                                var truncateTableMethod = daoType.GetMethods().Single(m => m.Name == "TruncateTable");
                                truncateTableMethod.Invoke(dao, new object[] { true });
                            }

                            // parse json
                            Newtonsoft.Json.Linq.JArray json = (Newtonsoft.Json.Linq.JArray)Newtonsoft.Json.JsonConvert.DeserializeObject(File.ReadAllText(fileName));

                            var headers = json.First().ToList();
                            List<object> entities = new List<object>();
                            var pbar = new ProgressBar(json.Count());
                            bars.Add(pbar);
                            pbar.Refresh(0, $"[  ] {dbEntityType.Name}");
                            for (int i = 1; i < json.Count(); i++) // skip header
                            {
                                // instanciate new db entity
                                var dbEntity = Activator.CreateInstance(dbEntityType);

                                for (var j = 0; j < headers.Count(); j++) // headers contains dbEntity's property names
                                {

                                    // find dbEntity's property
                                    string headerValue = headers[j].Value<string>();
                                    var props = dbEntityType.GetProperties();

                                    var prop = props.SingleOrDefault(p => p.Name.ToLower().Equals(headerValue.ToLower()));

                                    if (prop != null)
                                    {
                                        var column = columns.Single(c => string.Compare(c.Name, prop.Name, true) == 0);

                                        if (column.AutoIncrement)
                                            continue; // do not include autoincrement columns

                                        // assign its value

                                        // /!\ exception for binary types, currently expressed as strings in the JSON file... // TOFIX
                                        dynamic token = json.ElementAt(i).ElementAt(j);
                                        dynamic value = token.Value;
                                        try
                                        {
                                            if (column.DBColumnType == DBColumnType.Binary)
                                            {
                                                if (value != null)
                                                    // binaries are expressed as string in the json
                                                    value = new Binary(new ASCIIEncoding().GetBytes((string)value));
                                                else
                                                    value = new Binary(null);
                                            }

                                            if (value != null)
                                            {
                                                Type temp = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;

                                                object safeValue = (value == null) ? null : Convert.ChangeType(value, temp);
                                                prop.SetValue(dbEntity, safeValue, null);
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            throw new DataBaseException($"Import of entities '{dbEntityType.Name}' failed : Error when attempting to set value of property '{prop.Name}'value from Json data at row {i} column {j}. See InnerException for details.", e);
                                        }

                                    }
                                    else
                                    {
                                        throw new DataBaseException($"Failed to import value from header {headerValue} : No property with this name found in class {dbEntityType.Name}");
                                    }

                                }

                                entities.Add(dbEntity);
                                pbar.Next($"[ ] {dbEntityType.Name}");
                            }

                            daoType.InvokeMember("Add", BindingFlags.InvokeMethod, null, dao, new object[] { entities, null, null, null });
                            pbar.Refresh(json.Count, $"[\u221A] {dbEntityType.Name} ");

                        }
                    

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($" > Error: {e.ToString()}");
                    }

                });
                t.Start();
                tasks.Add(t);

            }
            Task.WaitAll(tasks.ToArray());
        }

        public static Type GetListOfEntityType(Type dbEntityType)
        {
            Type daoTypeGen = typeof(List<>);
            Type[] typeArgs = { dbEntityType };
            return daoTypeGen.MakeGenericType(typeArgs);
        }

        public static Type GetDaoForDBEntity(Type dbEntityType, bool throwErrorIfNotFound = true)
        {
            //// find the DAO for this entity type
            //var daoTypes = System.Reflection.Assembly.GetExecutingAssembly().GetTypes().Where(t => t.Namespace == "CellAO.Database.Dao" && (t.BaseType != null && t.BaseType.Name == "Dao`1"));
            // var daoType = daoTypes.SingleOrDefault(t => t.BaseType.GenericTypeArguments[0] == dbEntityType);

            Type daoType = null;

            //  CharacterDao : Dao<DBCharacter, CharacterDao>
            if ((dbEntityType.BaseType != null) && dbEntityType.BaseType.Name.Equals("DBEntity`1"))
            {
                // find any Dao which first subtype is our entityType
                var daoTypes = Assembly.GetExecutingAssembly().ExportedTypes.Where(t => t.Namespace == "CellAO.Database.Dao");

                var A = daoTypes.Where(t => (t.BaseType != null && t.BaseType.Name.StartsWith("Dao")));
                var B = A.Where(t => t.BaseType.GenericTypeArguments.FirstOrDefault() == dbEntityType);

                daoType = (Type)B.FirstOrDefault();
            }

            if (daoType == null)
            {
                // get a generic Dao
                Type daoTypeGen = typeof(Dao.Dao<>);
                Type[] typeArgs = { dbEntityType };
                daoType = daoTypeGen.MakeGenericType(typeArgs);
            }

            if (daoType == null)
                throw new DataBaseException($"Failed to find a Dao type for entity Type {dbEntityType.Name}");

            return daoType;
        }

        //[Obsolete("TODO: Revisit data loading logic")]
        //private static void FillTable(string tableName, bool truncateTable = false)
        //{
        //    if (truncateTable)
        //    {
        //        using (IDbConnection conn = Connector.GetConnection())
        //        {
        //            conn.Query($"DROP TABLE `{tableName}`;");
        //        }
        //    }


        //    // find json files in /Data directory
        //    var jsonFiles = Directory.GetFiles(Path.Combine(Directory.GetCurrentDirectory(), "Data"), "*.json");
        //    foreach (var jsonFile in jsonFiles)
        //    {
        //        var data = Newtonsoft.Json.JsonConvert.DeserializeObject<string[]>(File.ReadAllText(jsonFile));
        //    }


        //using (IDbConnection conn = Connector.GetConnection())
        //{
        //    Colouring.Push(ConsoleColor.Red);
        //    Console.Write("SQL Tables are not complete. Should they be created? (Y/N) ");
        //    Colouring.Pop();

        //    string answer = Console.ReadLine();
        //    string sqlQuery;
        //    if (answer.ToLower() == "y")
        //    {

        //        long fileSize = new FileInfo(fileName).Length;
        //        Colouring.Push(ConsoleColor.Green);
        //        Console.Write("Table " + tableName.PadRight(67) + "[  0%]");
        //        Colouring.Pop();
        //        if (fileSize > 10000)
        //        {
        //            string[] queries = File.ReadAllLines(fileName);
        //            int counter = 0;
        //            sqlQuery = string.Empty;
        //            string lastpercent = "0";
        //            while (counter < queries.Length)
        //            {
        //                if (queries[counter].IndexOf("INSERT INTO") == -1)
        //                {
        //                    sqlQuery += queries[counter] + "\n";
        //                }
        //                else
        //                {
        //                    counter--;
        //                    break;
        //                }

        //                counter++;
        //            }
        //            try
        //            {
        //                conn.Execute(sqlQuery);
        //            }
        //            catch (Exception)
        //            {
        //                Console.WriteLine(sqlQuery);
        //                throw;
        //            }

        //            counter++;
        //            string buf1 = string.Empty;
        //            while (counter < queries.Length)
        //            {
        //                if (queries[counter].ToLower().Substring(0, 11) == "insert into")
        //                {
        //                    break;
        //                }

        //                counter++;
        //            }

        //            if (counter < queries.Length)
        //            {
        //                buf1 = queries[counter].Substring(
        //                    0,
        //                    queries[counter].ToLower().IndexOf("values"));
        //                buf1 = buf1 + "VALUES ";
        //                StringBuilder Buffer = new StringBuilder(0, 1 * 1024 * 1024);
        //                while (counter < queries.Length)
        //                {
        //                    if (Buffer.Length == 0)
        //                    {
        //                        Buffer.Append(buf1);
        //                    }

        //                    string part = string.Empty;
        //                    while (counter < queries.Length)
        //                    {
        //                        if (queries[counter].Trim() != string.Empty)
        //                        {
        //                            part =
        //                                queries[counter].Substring(
        //                                    queries[counter].ToLower().IndexOf("values"));
        //                            part = part.Substring(part.IndexOf("(")); // from '(' to end
        //                            part = part.Substring(0, part.Length - 1); // Remove ';'
        //                            if (Buffer.Length + 1 + part.Length > 1024 * 1000)
        //                            {
        //                                Buffer.Remove(Buffer.Length - 2, 2);
        //                                Buffer.Append(";");
        //                                try
        //                                {
        //                                    conn.Execute(Buffer.ToString());
        //                                }
        //                                catch (Exception)
        //                                {
        //                                    Console.WriteLine(Buffer.ToString().Substring(0, 300));
        //                                    throw;
        //                                }

        //                                Buffer.Clear();
        //                                Buffer.Append(buf1);
        //                                string lp2 =
        //                                    Convert.ToInt32(
        //                                        Math.Floor((double)counter / queries.Length * 100))
        //                                        .ToString();
        //                                if (lp2 != lastpercent)
        //                                {
        //                                    Console.Write(
        //                                        "\rTable " + fileName.PadRight(67) + "[" + lp2.PadLeft(3)
        //                                        + "%]");
        //                                    lastpercent = lp2;
        //                                }
        //                            }

        //                            Buffer.Append(part + ", ");
        //                        }

        //                        counter++;
        //                    }

        //                    Buffer.Remove(Buffer.Length - 2, 2);
        //                    Buffer.Append(";");
        //                    conn.Execute(Buffer.ToString());
        //                    Buffer.Clear();
        //                    string lp =
        //                        Convert.ToInt32(Math.Floor((double)counter / queries.Length * 100))
        //                            .ToString();
        //                    if (lp != lastpercent)
        //                    {
        //                        Console.Write(
        //                            "\rTable " + fileName.PadRight(67) + "[" + lp.PadLeft(3) + "%]");
        //                        lastpercent = lp;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                Colouring.Push(ConsoleColor.Green);
        //                Console.Write("\rTable " + fileName.PadRight(67) + "[100%]");
        //                Colouring.Pop();
        //            }
        //        }
        //        else
        //        {
        //            sqlQuery = File.ReadAllText(fileName);
        //            conn.Execute(sqlQuery);
        //            Colouring.Push(ConsoleColor.Green);
        //            Console.Write("\rTable " + fileName.PadRight(67) + "[100%]");
        //            Colouring.Pop();
        //        }

        //        Console.WriteLine();
        //    }
        //}

        //}

    }

}
