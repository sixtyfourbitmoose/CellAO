﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Core.Inventory
{
    #region Usings ...

    using System;
    using System.Collections.Generic;
    using System.Linq;

    using CellAO.Core.Entities;
    using CellAO.Core.Items;
    using CellAO.Database.Dao;
    using CellAO.Enums;
    using CellAO.ObjectManager;

    using CellAO.Interfaces;
    using CellAO.Messages.Fields;
    using CellAO.Database.Entities;

    #endregion

    /// <summary>
    /// </summary>
    public abstract class BaseInventoryPage : PooledObject, IInventoryPage
    {
        #region Fields

        /// <summary>
        /// </summary>
        private readonly IDictionary<int, IItem> Content = new Dictionary<int, IItem>();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// </summary>
        /// <param name="pagenum">
        /// </param>
        /// <param name="maxslots">
        /// </param>
        /// <param name="firstslotnumber">
        /// </param>
        /// <param name="ownerInstance">
        /// </param>
        public BaseInventoryPage(int pagenum, int maxslots, int firstslotnumber, Identity ownerInstance)
            : base(ownerInstance, new Identity() { Type = (IdentityType)pagenum, Instance = ownerInstance.Instance })
        {
            this.MaxSlots = maxslots;
            this.FirstSlotNumber = firstslotnumber;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// </summary>
        public int FirstSlotNumber { get; set; }

        /// <summary>
        /// </summary>
        public int MaxSlots { get; set; }

        /// <summary>
        /// </summary>
        public bool NeedsItemCheck { get; set; }

        /// <summary>
        /// </summary>
        public virtual int Page
        {
            get
            {
                return (int)this.Identity.Type;
            }

            // Commenting this for now, we probably wont need it
            /*set
            {
                this.Identity.Type = (IdentityType)value;
            }*/
        }

        #endregion

        #region Public Indexers

        /// <summary>
        /// </summary>
        /// <param name="index">
        /// </param>
        /// <returns>
        /// </returns>
        public IItem this[int index]
        {
            get
            {
                if (this.Content.ContainsKey(index))
                {
                    return this.Content[index];
                }

                return null;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// </summary>
        /// <param name="slot">
        /// </param>
        /// <param name="item">
        /// </param>
        /// <returns>
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public virtual InventoryError Add(int slot, IItem item)
        {
            if (this.Content.ContainsKey(slot))
            {
                throw new ArgumentException(
                    "Already item in slot " + slot + " of container " + this.Identity.Type + ":"
                    + this.Identity.Instance);
            }

            if ((slot < this.FirstSlotNumber) || (slot > this.FirstSlotNumber + this.MaxSlots))
            {
                throw new ArgumentOutOfRangeException(
                    "Slot out of range: " + slot + " not in " + this.FirstSlotNumber + " to "
                    + (this.FirstSlotNumber + this.MaxSlots));
            }

            // Step 1 is make the change in memory
            this.Content.Add(slot, item);

            this.Write();
            
            
            return InventoryError.OK;
        }

        /// <summary>
        /// </summary>
        /// <param name="character">
        /// </param>
        public virtual void CalculateModifiers(Character character)
        {
            // Do nothing
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        public int FindFreeSlot()
        {
            int slot = this.FirstSlotNumber;
            while (slot < this.FirstSlotNumber + this.MaxSlots)
            {
                if (!this.Content.ContainsKey(slot))
                {
                    return slot;
                }

                slot++;
            }

            return -1;
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        public IDictionary<int, IItem> List()
        {
            return this.Content;
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        public virtual bool Read()
        {
            int containerType = (int)this.Identity.Type;

            // omg, i forgot to clear before read... - Algorithman
            this.Content.Clear();

            foreach (DBItem item in ItemDao.Instance.GetAllInContainer(containerType, this.Identity.Instance))
            {
                Item newItem = new Item(item.Quality, item.LowId, item.HighId);
                newItem.SetAttribute(412, item.MultipleCount);
                newItem.IsInstanced = false;
                newItem.Identity = new Identity() { Instance = item.Id, Type = IdentityType.None};
                this.Content.Add(item.ContainerPlacement, newItem);

                // Make item visible
                // TODO: Other flags must be set too
                newItem.Flags |= 0x1;
            }

            foreach (
                DBInstancedItem item in
                    Dao<DBInstancedItem>.Instance.GetAll(
                        new { containertype = containerType, containerinstance = this.Identity.Instance }))
            {
                Item newItem = new Item(item.Quality, item.LowId, item.HighId);
                newItem.SetAttribute(412, item.MultipleCount);
                Identity temp = new Identity();
                temp.Type = IdentityType.InstancedItem;
                temp.Instance = item.Id;

                switch(newItem.ItemClass)
                {

                    // TODO: Add other Item class to IdentityType mappings here as they become needed

                    case ItemType.Weapon:
                        temp.Type = IdentityType.WeaponInstance;
                        break;
                }

                newItem.IsInstanced = true;
                newItem.Identity = temp;

                byte[] binaryStats = item.Stats.ToArray();
                for (int i = 0; i < binaryStats.Length / 8; i++)
                {
                    int statid = BitConverter.ToInt32(binaryStats, i * 8);
                    int statvalue = BitConverter.ToInt32(binaryStats, i * 8 + 4);
                    newItem.SetAttribute(statid, statvalue);
                }

                // Make item visible
                // TODO: Other flags must be set too
                // Anything ->    =0x01
                // Containers ->  =0x02
                // ????? ->       |0x20
                // ????? ->       |0x80 (maybe unique)

                // Found online: 0xa1 for nano instruction disc
                // 0x02 for any bag
                // 0x81 for unique totw rings
                newItem.Flags |= 0x1;
                this.Content.Add(item.ContainerPlacement, newItem);
            }

            return true;
        }

        /// <summary>
        /// </summary>
        /// <param name="slotNum">
        /// </param>
        /// <returns>
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IItem Remove(int slotNum, bool persistToDb = false)
        {
            // TODO: Item placement switches could cause items to disappear when zoneengine crashes at that moment
            if (!this.Content.ContainsKey(slotNum) && 1!=1)
            {
                throw new ArgumentOutOfRangeException(
                    "No item in slot " + slotNum + " of container " + this.Identity.Type + ":" + this.Identity.Instance);
            }
            IItem temp = this.Content[slotNum];
            if (persistToDb)
            {
                int containerType = (int)this.Identity.Type;

                if (!temp.IsInstanced)
                {
                    ItemDao.Instance.Delete(
                        new
                        {
                            containertype = containerType,
                            containerinstance = this.Identity.Instance,
                            containerplacement = slotNum
                        });
                }
                else
                {
                    Dao<DBInstancedItem>.Instance.Delete(
                        new
                        {
                            containertype = containerType,
                            containerinstance = this.Identity.Instance,
                            containerplacement = slotNum
                        });
                }

            ((Item)temp).Identity = new Identity() { Type = temp.Identity.Type, Instance = 0 };
            }

            this.Content.Remove(slotNum);
            return temp;
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        public BankSlot[] ToInventoryArray()
        {
            List<BankSlot> slots = new List<BankSlot>();
            foreach (KeyValuePair<int, IItem> kv in this.List())
            {
                short flags = 0;
                if (kv.Value.IsInstanced)
                {
                    flags = 0xa0;
                }

                flags |= (short)((kv.Value.LowID == kv.Value.HighID) ? 2 : 1);
                var slot = new BankSlot();
                slot.Flags = flags;
                slot.Count = (short)kv.Value.MultipleCount;
                slot.Identity = kv.Value.Identity;
                slot.ItemLowId = kv.Value.LowID;
                slot.ItemHighId = kv.Value.HighID;
                slot.Quality = kv.Value.Quality;
                slot.ItemFlags = 0;
                slots.Add(slot);
            }

            return slots.ToArray();
        }

        /// <summary>
        /// </summary>
        /// <param name="slotNum">
        /// </param>
        /// <returns>
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool ValidSlot(int slotNum)
        {
            return (this.FirstSlotNumber <= slotNum) && (slotNum < this.FirstSlotNumber + this.MaxSlots);
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        public virtual bool Write()
        {
            foreach (KeyValuePair<int, IItem> kv in this.Content)
            {
                if (kv.Value.IsInstanced)
                {
                    var row = Dao<DBInstancedItem>.Instance.GetAll(new { ContainerType = (int)this.Identity.Type, ContainerInstance = this.Identity.Instance, ContainerPlacement = kv.Key }).FirstOrDefault();
                    
                    if (row == null)
                    {
                        var i = kv.Value.ToDBInstancedItem(this.Identity.Instance, (int)this.Identity.Type, kv.Key);
                        Dao<DBInstancedItem>.Instance.Add(i, null, null);
                        ((Item)kv.Value).Identity = new Identity() { Instance = i.Id, Type = kv.Value.Identity.Type };
                    }
                    else
                    {
                        Dao<DBInstancedItem>.Instance.Save(kv.Value.ToDBInstancedItem(this.Identity.Instance, (int)this.Identity.Type, kv.Key), null, null);
                    }
                }
                else
                {
                    var row = ItemDao.Instance.GetAll(new { ContainerType = (int)this.Identity.Type, ContainerInstance = this.Identity.Instance, ContainerPlacement = kv.Key }).FirstOrDefault();
                    if (row == null)
                    {
                        var i = kv.Value.ToDBItem(this.Identity.Instance, (int)this.Identity.Type, kv.Key);
                        ItemDao.Instance.Add(i, null, null);
                        ((Item)kv.Value).Identity = new Identity() { Instance = i.Id, Type = kv.Value.Identity.Type };
                    }
                    else
                    {
                        ItemDao.Instance.Save(kv.Value.ToDBItem(this.Identity.Instance, (int)this.Identity.Type, kv.Key), null, null);
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// </summary>
        /// <param name="item">
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Added(ItemTemplate item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// </summary>
        /// <param name="slot">
        /// </param>
        /// <param name="item">
        /// </param>
        /// <param name="err">
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void CheckAdd(int slot, ItemTemplate item, InventoryError err)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// </summary>
        /// <param name="slot">
        /// </param>
        /// <param name="templ">
        /// </param>
        /// <param name="err">
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void CheckRemove(int slot, ItemTemplate templ, InventoryError err)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// </summary>
        /// <param name="sendingPage">
        /// </param>
        /// <param name="fromPlacement">
        /// </param>
        /// <param name="toPlacement">
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Equip(IInventoryPage sendingPage, int fromPlacement, int toPlacement)
        {
            IItem toEquip = sendingPage[fromPlacement];

            // First: Check if the item can be worn
            bool canBeWornCheck = (toEquip.GetAttribute(30) & (int)CanFlags.Wear) == (int)CanFlags.Wear;

            if (canBeWornCheck)
            {
                sendingPage.Remove(fromPlacement, true);
                this.Add(toPlacement, toEquip);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="sendingPage">
        /// </param>
        /// <param name="fromPlacement">
        /// </param>
        /// <param name="toPlacement">
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void HotSwap(IInventoryPage sendingPage, int fromPlacement, int toPlacement)
        {
            IItem toEquip = sendingPage[fromPlacement];
            IItem hotSwapItem = this[toPlacement];

            sendingPage.Remove(fromPlacement, true);
            this.Remove(toPlacement, true);

            // zero out the in memory items so that when the persistence kicks in it knows to recreate the entries 

            // ((Item)toEquip).Identity = new Identity() { Type = toEquip.Identity.Type };
            // ((Item)hotSwapItem).Identity = new Identity() { Type = hotSwapItem.Identity.Type };

            sendingPage.Add(fromPlacement, hotSwapItem);
            this.Add(toPlacement, toEquip);
        }

        /// <summary>
        /// </summary>
        /// <param name="slot">
        /// </param>
        /// <param name="item">
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Removed(int slot, ItemTemplate item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// </summary>
        /// <param name="slotFrom">
        /// </param>
        /// <param name="slotTo">
        /// </param>
        /// <param name="err">
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void TryHotSwap(int slotFrom, int slotTo, InventoryError err)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// </summary>
        /// <param name="fromPlacement">
        /// </param>
        /// <param name="receivingPage">
        /// </param>
        /// <param name="toPlacement">
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Unequip(int fromPlacement, IInventoryPage receivingPage, int toPlacement)
        {
            IItem toUnEquip = this[fromPlacement];
            this.Remove(fromPlacement, true);
            receivingPage.Add(toPlacement, toUnEquip);
        }

        #endregion
    }
}