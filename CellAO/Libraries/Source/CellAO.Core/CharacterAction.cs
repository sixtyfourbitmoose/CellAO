﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellAO.Core
{
    using CellAO.Enums;
    using CellAO.Core.Entities;
    using CellAO.Messages.Fields;



    public class CharacterAction : ITimedAction
    {
        public ICharacter Character { get; set; }
        public ICharacter TargetCharacter { get; set; }
        public CharacterActionTypes Type { get; set; }
        public int AttackCycleRemaining { get; set; }

        public int Id { get; set; }

        public Nanos.NanoFormula Nano { get; set; }

        public bool Paused { get; set; }

        public bool Processed { get; set; }

        public int RechargeCycleRemaining { get; set; }

        public void Execute()
        {
            switch (Type)
            {
                case CharacterActionTypes.LeftAttack:
                    if (TargetCharacter.Controller.IsDead()) return;
                    if (TargetCharacter.DistanceFrom(Character) > Character.GetLeftHandWeapon().GetAttribute(287))
                    {

                        Utility.LogUtil.Debug(Utility.DebugInfoDetail.Combat, String.Format("[{0}] Target [{1}] is out of range", Character.Name, TargetCharacter.Name));

                        if (Character.CharacterType == CharacterType.NPC)
                        {
                            // Run to closer range (melee or ranged)
                            // follow the target and pause combat

                            Character.ActionsQueue.ForEach(act => act.Paused = true);

                            // This logic needds to be pulled out of CharacterAction and put into NPCController

                            if (!((ActionFlags)Character.Stats[(int)StatIds.flags].Value).HasFlag(ActionFlags.Movement)) 
                            {
                                Utility.LogUtil.Debug(Utility.DebugInfoDetail.Combat, String.Format("[{0}] Attemping to run close into range of ", Character.Name, TargetCharacter.Name));

                                Character.Playfield.Announce(new Messages.N3Messages.FollowTargetMessage()
                                {
                                    Identity = Character.Identity,

                                    N3MessageType = Messages.N3MessageType.FollowTarget,
                                    Unknown = 0,
                                    Info = new FollowCoordinateInfo()
                                    {
                                        MoveMode = 25,
                                        CoordinateCount = 2,
                                        CurrentCoordinates = Character.RawCoordinates,
                                        EndCoordinates = TargetCharacter.RawCoordinates,
                                        FollowInfoType = 1
                                    }
                                });
                                Character.SetCoordinates(TargetCharacter.Coordinates(), TargetCharacter.RawHeading);
                            }

                        }
                        return;
                    }

                    // Now in range... stop following
                    if (Character.CharacterType == CharacterType.NPC)
                    {
                        Character.Controller.StopFollow();
                        Character.ActionsQueue.ForEach(act => act.Paused = false);
                    }

                    Character.DoAttack(8, TargetCharacter);
                    break;
                case CharacterActionTypes.RightAttack:
                    if (TargetCharacter.Controller.IsDead()) return;
                    if (TargetCharacter.DistanceFrom(Character) > Character.GetRightHandWeapon().GetAttribute(287))
                    {
                        Utility.LogUtil.Debug(Utility.DebugInfoDetail.Combat, String.Format("[{0}] Target [{1}] is out of range", Character.Name, TargetCharacter.Name));
                        if (Character.CharacterType == CharacterType.NPC)
                        {
                            // Run to closer range (melee or ranged)
                            // follow the target and pause combat
                            
                            Character.ActionsQueue.ForEach(act => act.Paused = true);

                            // This logic needds to be pulled out of CharacterAction and put into NPCController

                            if (!((ActionFlags)Character.Stats[(int)StatIds.flags].Value).HasFlag(ActionFlags.Movement))
                            {

                                Utility.LogUtil.Debug(Utility.DebugInfoDetail.Combat, String.Format("[{0}] Attemping to run close into range of ", Character.Name, TargetCharacter.Name));

                                Character.Playfield.Announce(new Messages.N3Messages.FollowTargetMessage()
                                {
                                    Identity = Character.Identity,

                                    N3MessageType = Messages.N3MessageType.FollowTarget,
                                    Unknown = 0,
                                    Info = new FollowCoordinateInfo()
                                    {
                                        MoveMode = 25,
                                        CoordinateCount = 2,
                                        CurrentCoordinates = Character.RawCoordinates,
                                        EndCoordinates = TargetCharacter.RawCoordinates,
                                        FollowInfoType = 1
                                    }
                                });
                                Character.SetCoordinates(TargetCharacter.Coordinates(), TargetCharacter.RawHeading);
                            }

                        }
                        return;
                    }

                    // Now in range... stop following
                    if (Character.CharacterType == CharacterType.NPC)
                    {
                        Utility.LogUtil.Debug(Utility.DebugInfoDetail.Combat, String.Format("[{0}] now within range of ", Character.Name, TargetCharacter.Name));

                        Character.Controller.StopFollow();
                        Character.ActionsQueue.ForEach(act => act.Paused = false);
                    }
                    
                    Character.DoAttack(6, TargetCharacter);
                    break;
                case CharacterActionTypes.Despawn:
                    Character.Playfield.Despawn(Character.Identity);
                    break;
                case CharacterActionTypes.StopAttack: // Is this needed? does it have to be queued?
                    Character.DoStopAttack();
                    break;
                case CharacterActionTypes.Dispose:
                    Character.ActionsQueue.Clear();
                    break;
                case CharacterActionTypes.CastNano:
                    Character.Controller.FinishCastingNano(this);
                    break;
            }
        }

        public void FinishedRecharge()
        {
            switch (Type)
            {
                case CharacterActionTypes.LeftAttack:
                    Character.StartAttackCycle(8, TargetCharacter);
                    break;
                case CharacterActionTypes.RightAttack:
                    Character.StartAttackCycle(6, TargetCharacter);
                    break;
            }
        }
    }
}
