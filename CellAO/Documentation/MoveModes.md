# MoveModes #
`CellAO.Enums.MoveModes`  

----------


**None**,

**Rooted**,

**Walk**,

**Run**,

**Swim**,

**Crawl**,

**Sneak**,

**Fly**,

**Sit**,

**SocialTemp**,

**Nothing**,

**Sleep**,

**Lounge**


----------

*Copyright © 2016 CellAO Team*

*Created by MarkDownDocumentator Version 1.5.0.36236 - Phoenix*


